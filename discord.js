const config = require('./config')
const { broadcastIsPlaying, generateRandomAudioFile } = require('./audio')
const { runServerCommand, handleDmMessage } = require('./commands')
const Global = require('./Global')
const { voiceRecognitionStream } = require('./speech')
const { getGitBranchName, getDiskInfo, printLog, printError, sleep } = require('./system')
const Event = require('./Events').eventBus

const discord_token = config.getDiscordToken()
const discordClient = Global.discordClient
const joinChannelMin = (process.argv.indexOf('--two_min') <= -1) ? 1 : 2

var vc0 = null
var vc1 = null
var vc2 = null
var vc3 = null
var vc4 = null

var statusUpdateInvolvesBotOwner

discordClient.on('ready', async () => {
  try {
    const branchName = await getGitBranchName()
    await setPresence((branchName === 'master' ? 'online' : 'dnd'))
  } catch (error) {
    throw error
  }
  getDiskInfo()
  const guildIds = getListOfAvailableServers(discordClient)
  // process.exit(33)
  printLog(`Logged in as ${discordClient.user.tag}!`)
  for(guildId of guildIds) {
    const guild = discordClient.guilds.cache.get(guildId)
    Global.availableGuilds.push(guild)
  }
  joinChannelLogic()
})

discordClient.on('voiceStateUpdate', async (oldState, newState) => {
  const botOwner = config.getBotOwner()
  const updateInvolvesBotVoiceChannel = typeof Global.listeningVoiceConnections.filter(connection => connection.channel.id === oldState.channelID).map(connection => connection.channel)[0] !== 'undefined'
  if(oldState.id === discordClient.user.id || newState.id === discordClient.user.id) { return }
  var isSelfInChannel = false
  if(oldState.channelID === null && newState.channelID !== null) { // user was not in channel before, but is now
    const newStateVoiceChannel = getVoiceChannel(newState.channelID)
    const newStateSelfInChannel = newStateVoiceChannel.members.filter((member) => { return member.user.bot === true && member.user.id === discordClient.user.id })
    const membersInChannel = newStateVoiceChannel.members.filter((member) => { return member.user.bot === false })
    const availableVoiceChannels = Global.getListOfAvailableVoiceChannelsInServer(newState.guild)
    for(voiceChannel of availableVoiceChannels) {
      const selfInThisChannel = voiceChannel.members.filter((member) => { return member.user.bot === true && member.user.id === discordClient.user.id })
      if(selfInThisChannel.size > 0 || newStateSelfInChannel.size > 0) {
        isSelfInChannel = true
        break
      }
    }
    if(membersInChannel.size >= joinChannelMin) {
      if(!isSelfInChannel) {
        statusUpdateInvolvesBotOwner = (oldState.member.user.id === botOwner || newState.member.user.id === botOwner)
        joinChannelLogic(newState.guild.id, newStateVoiceChannel.id)
      }
      else {
        const connections = discordClient.voice.connections.map((connection => connection))
        for(connection of connections) {
          if(connection.channel.id === newStateVoiceChannel.id) {
            statusUpdateInvolvesBotOwner = (oldState.member.user.id === botOwner || newState.member.user.id === botOwner)
            await sleep(1000)
            if(statusUpdateInvolvesBotOwner) {
              generateRandomAudioFile(connection, 'user_joined_channel/bot_owner')
              statusUpdateInvolvesBotOwner = false
            }
            else {
              generateRandomAudioFile(connection, 'user_joined_channel')
            }
            break
          }
        }
      }
    }
  }
  else if(oldState.channelID !== null && newState.channelID === null) { // user was in channel before, but has now left
    const oldStateVoiceChannel = getVoiceChannel(oldState.channelID)
    const membersInChannel = oldStateVoiceChannel.members.filter((member) => { return member.user.bot !== true })
    if(membersInChannel.size < joinChannelMin && updateInvolvesBotVoiceChannel) {
      leaveChannel(oldStateVoiceChannel)
    }
  }
  else if(oldState.channelID !== null && newState.channelID !== null) { // user was already in a channel before, most likely moved to another channel in server
    const oldStateVoiceChannel = getVoiceChannel(oldState.channelID)
    const newStateVoiceChannel = getVoiceChannel(newState.channelID)
    const availableVoiceChannels = Global.getListOfAvailableVoiceChannelsInServer(newState.guild)
    for(voiceChannel of availableVoiceChannels) {
      const selfInChannel = voiceChannel.members.filter((member) => { return member.user.bot === true && member.user.id === discordClient.user.id })
      if(selfInChannel.size > 0) {
        isSelfInChannel = true
        break
      }
    }
    const oldStateMembers = oldStateVoiceChannel.members.filter((member) => { return member.user.bot === false })
    const newStateMembers = newStateVoiceChannel.members.filter((member) => { return member.user.bot === false })
    if(oldStateMembers.size < newStateMembers.size && newStateVoiceChannel.guild.afkChannelID !== newStateVoiceChannel.id) {
      if(isSelfInChannel) {
        const connections = discordClient.voice.connections.map((connection => connection))
        var userJoinedChannel = false
        for(connection of connections) {
          if(connection.channel.id === newStateVoiceChannel.id) {
            await sleep(1000)
            statusUpdateInvolvesBotOwner = (oldState.member.user.id === botOwner || newState.member.user.id === botOwner)
            if(statusUpdateInvolvesBotOwner) {
              generateRandomAudioFile(connection, 'user_joined_channel/bot_owner')
              statusUpdateInvolvesBotOwner = false
            }
            else {
              generateRandomAudioFile(connection, 'user_joined_channel')
            }
            userJoinedChannel = true
            break
          }
        }
        if(!userJoinedChannel && updateInvolvesBotVoiceChannel) {
          statusUpdateInvolvesBotOwner = (oldState.member.user.id === botOwner || newState.member.user.id === botOwner)
          joinChannelLogic(newState.guild.id, newStateVoiceChannel.id)
        }
      }
      else if(oldStateMembers.size !== newStateMembers.size) {
        joinChannelLogic(newState.guild.id, newStateVoiceChannel.id)
      }
    }
    else if(oldStateVoiceChannel.guild.afkChannelID === oldStateVoiceChannel.id) {
      joinChannelLogic(newState.guild.id, newStateVoiceChannel.id)
    }
    else if(oldStateMembers.size < newStateMembers.size && oldStateMembers.size < joinChannelMin) {
      leaveChannel(oldStateVoiceChannel)
    }
  }
})

discordClient.on('message', async (message) => {
  if(message.member != null && message.channel.type === 'text') {
    const channel = message.channel
    const msg = message.content.substring(1)
    const msgArray = msg.match(/[^\s]+/g)
    const musicBotPrefix = config.getMusicBotPrefix(message.guild.id)
    const server = config.getServer(message.guild.id)
    const queueCommand = server.queue_command
    const selfPrefix = config.getSelfPrefix()
    if(message.content.startsWith(`${musicBotPrefix}${queueCommand}`)) {
      const msg = message.content.substring(1)
      const msgArray = msg.match(/[^\s]+/g)
      const phrase = msgArray.slice(1).join(' ')
      const bannedSongs = config.getBannedSongs()
      for(song of bannedSongs) {
        if(phrase === song) {
          const stopCommand = server.stop_command
          await sleep(2000)
          await channel.send(`${musicBotPrefix}${stopCommand}`)
          return
        }
      }
    }
    else if(message.content.startsWith(selfPrefix)) {
      const user = message.member.user
      const cmd = msgArray[0]
      const args = msgArray.slice(1)
      await runServerCommand(cmd, args, user, channel)
    }
  }
  else if(message.channel.type === 'dm' && message.author.id !== discordClient.user.id) {
    const user = message.author
    const msg = message.content
    const attachments = message.attachments
    handleDmMessage(user, msg, attachments)
  }
})

// joined a server
discordClient.on('guildCreate', async (guild) => {
  await addServer(guild.id)
  await setAdminUser(guild.ownerID)
  // Send message to available text channel (aka. channel with permissions to send) to let users know how to setup bot for proper functionality
  // If no text channel is available, send message to server owner instead
  const selfPrefix = config.getSelfPrefix()
  const message0 = `Hi! My name is **${Global.discordClient.user.username}** and I'm here to help become an extension of your existing music bot. Thanks for adding me to your server! Before you can begin speaking to me, there are a few things you should know:
  **1.** You must tell a few things about this server. Specifically, the channel you use for music commands, which bot is your music bot, the name of your bot, your music bot's prefix character, and the commands you use to queue music, skip, and stop.
    **a.** If you haven't set which user(s) can enter the commands to tell me that information, then only the server owner can enter/change these commands. Enter ${selfPrefix} add help for more information
  **2.** The default prefix for me is ${selfPrefix}. This cannot be changed so if your music bot uses the same prefix, you might want to consider changing it
  **3.** I have a built in voice encoder that can save recordings of any one person who is talking. This encoder is set to OFF by DEFAULT for everyone. If you know what my creator made this for and you want it to be set to ON, there is a command for that. See ${selfPrefix} help for more information
    **a.** FULL DISCLOSURE: I do not send any voice data that I encode to any 3rd parties. Only voice data at the time of speaking is being sent to ether Google, Microsoft, or IBM_Watson and is completely AUTONOMOUS. Vosk is the four speech engine, but that runs locally on the server
  **4.** If you notice any issues with me, you can send a report to me using ${selfPrefix} report and I'll make sure my creator gets the message.
  **5.** Every Saturday at ~9:00 pm there's a surprise broadcast that gets played across every server I am in. There is no way to disable the broadcast and if you or anybody else in the server doesn't like it, then you should honestly ask yourself if you have any real value on this planet :blush:
  **6.** I like certain words/phrases, I hate certain words/phrases. See if you can find out which ones they are
  `
  const message1 = `
  **7.** If you currently don't have a music bot, I recommend adding this bot (https://discord.com/oauth2/authorize?client_id=406969368489099266&scope=bot&permissions=0) to the server, particularly because it's also managed by my creator and was tested to make sure it worked well with me
  **8.** If you do have a music bot and it is not listening to music commands sent by me, it's probably because that bot ignores any and all bots. HOW RASIST!! If that's the case, you'll have to go to your music bot's code and find where it's ignoring messages from other bots (if you host it on your own server), contact your bot admins to see if they can make those changes, or use what I suggests above
  **Note:** 
  **9.** If you do not plan to speak to me, please try to use the 'leave' command. I try to outsource my understanding of human language from other (better) sources that only give a certain amount of free time before they want to charge me per minute before I use my own voice engine.
  - My understanding of human speech is not perfect, hence the reason why I have four Speech Recognition engines built in (Google, Microsoft, IBM Watson, and Vosk). Every one of theses is used at some point in a specific way that keeps the cost of running me at a fixed price
  - If you have any questions about me, you can use the 'report' command to message my creator, however, it's not guaranteed that he'll get back to you
  - ONE LAST IMPORTANT THING: MAKE SURE I HAVE BASIC PERMISSIONS TO SEND MESSAGES TO SOME TEXT CHANNEL BEFORE INTERACTING WITH ME IN ANY WAY
  
  When you're ready, try saying 'Hey Bimbo' to me!
  
  That's all for now! Once again, I thank you for having me over! It better be worth my time...
  `
  var welcomeMessageSent = false
  const textChannels = Global.getListOfAvailableTextChannelsInServer(guild)
  for(channel of textChannels) {
    if(channel.permissionsFor(Global.discordClient.user).has('SEND_MESSAGES', true)) {
      channel.send(message0)
      channel.send(message1)
      welcomeMessageSent = true
      break
    }
  }
  if(welcomeMessageSent === false) {
    await sendMessageToUser(guild.ownerID, message0)
    await sendMessageToUser(guild.ownerID, message1)
  }
  Global.availableGuilds.push(guild)
  joinChannelLogic(guild.id)
})

// removed from a server
discordClient.on('guildDelete', async (guild) => {
  await removeServer(guild.id)
})

discordClient.on('guildMemberSpeaking', (member, speaking) => {
  const serverMusicBotId = config.getMusicBotId(member.guild.id)
  if(member.user.id === serverMusicBotId) {
    Global.musicBotIsSpeaking = speaking.bitfield
  }
})

discordClient.login(discord_token)

async function setPresence(status) { // 'online' || 'idle' || 'dnd' || 'invisible'
  await discordClient.user.setPresence({
    status: status, 
    activity: {
      name: '',
      type: 'CUSTOM_STATUS', // other types: PLAYING | STREAMING | LISTENING | WATCHING | CUSTOM_STATUS | COMPETING
      url: ''
    }
  })
}

// ===================PUBLIC=================== //
function getVoiceChannel(channelId) {
  const guilds = Global.discordClient.guilds.cache.map((guild => guild))
  for(guild of guilds) {
    const voiceChannel = guild.channels.cache.filter(channel => channel.type === 'voice').get(channelId)
    if(typeof voiceChannel !== 'undefined') {
      return voiceChannel
    }
  }
  return null
}

function joinChannelLogic(guildId, channelId) {
  for(guild of Global.availableGuilds) {
    const voiceChannels = Global.getListOfAvailableVoiceChannelsInServer(guild)
    for(voiceChannel of voiceChannels) {
      const membersInChannel = voiceChannel.members.filter((member) => { return member.user.bot === false })
      if(membersInChannel.size > 0) {
        Global.guildMembers.push(membersInChannel)
      }
      if(membersInChannel.size >= joinChannelMin) {
        if(typeof guildId !== 'undefined' && typeof channelId !== 'undefined' && guildId === guild.id && channelId === voiceChannel.id) {
          joinChannel(voiceChannel)
          return
        }
        else if(typeof guildId !== 'undefined' && guildId === guild.id) {
          joinChannel(voiceChannel)
          return
        }
        else if(typeof guildId === 'undefined') {
          joinChannel(voiceChannel)
          break
        }
      }
    }
  }
}

function joinChannel(voiceChannel) {
  voiceChannel.join().then(async (connection) => {
    await sleep(1000)
    if(statusUpdateInvolvesBotOwner) {
      generateRandomAudioFile(connection, 'user_joined_channel/bot_owner')
      statusUpdateInvolvesBotOwner = false
    }
    else {
      generateRandomAudioFile(connection, 'bot_joined_channel')
    }
    onSpeakingCallback()
    printLog(`Listening to the ${connection.channel.guild.name} Server!`)
  }).catch(function(error) {
    printLog(`JOIN_CHANNEL_ERROR: ${error}`)
  })
}

function leaveChannel(voiceChannel) {
  const listeningConnections = Global.listeningVoiceConnections
  var voiceConnection = null
  for(var i = 0; i < listeningConnections.length; i++) {
    if(voiceChannel.guild.id === Global.listeningVoiceConnections[i].channel.guild.id) {
      voiceConnection = Global.listeningVoiceConnections[i]
      Global.listeningVoiceConnections.splice(i, 1)
      break
    }
  }
  if(voiceConnection !== null) {
    if(voiceConnection === vc0) {
      vc0 = null
    }
    else if(voiceConnection === vc1) {
      vc1 = null
    }
    else if(voiceConnection === vc2) {
      vc2 = null
    }
    else if(voiceConnection === vc3) {
      vc3 = null
    }
    else if(voiceConnection === vc4) {
      vc4 = null
    }
  }
  voiceChannel.leave()
}

async function sendMessageToServers(message) {
  const servers = config.getServers()
  for(server of servers) {
    try {
      const channel = await Global.fetchChannel(server.channel_id)
      await channel.send(message)
    } catch (e) {
      printError(`SEND_MESSAGE_TO_SERVERS_ERROR: ${e}`)
    }
  }
}

async function sendMessageToBotOwner(msg, opts) {
  const botOwner = config.getBotOwner()
  discordClient.users.fetch(botOwner, true).then((user) => {
    if(typeof opts !== 'undefined' && opts !== null) {
      user.send(msg, opts)
    }
    else {
      user.send(msg)
    }
  })
}

async function sendMessageToUser(userId, msg) {
  const user = await discordClient.users.fetch(userId, true)
  await user.send(msg)
}

async function punishUser(guildMember) {
  if(guildMember.guild.afkChannelID !== null) {
    await guildMember.voice.setChannel(guildMember.guild.afkChannelID)
  }
}

function getListOfAvailableServers(discordClient) {
  return discordClient.guilds.cache.map(guild => guild.id)
}

async function setVoiceListeningForUser(userEnableListening, userId) {
  var tempConfig = config.getConfig()
  var listenerList = config.getVoiceListeners()
  userEnableListening = userEnableListening === 'enable'
  if(!userEnableListening && listenerList.indexOf(userId) > -1) {
    for(var i = 0; i < listenerList.length; i++) {
      const listenerUserId = listenerList[i]
      if(userId === listenerUserId) {
        listenerList.splice(i, 1)
        tempConfig.listen_to_users = listenerList
        break
      }
    }
  }
  else if(userEnableListening && listenerList.indexOf(userId) <= -1) {
    listenerList.push(userId)
    tempConfig.listen_to_users = listenerList
  }
  await config.updateConfig(tempConfig)
}

async function setVoiceRecorderForUser(userEnableRecording, userId) {
  var tempConfig = config.getConfig()
  var recordUserList = config.getRecordUsers()
  userEnableRecording = userEnableRecording === 'enable'
  if(!userEnableRecording && recordUserList.indexOf(userId) > -1) {
    for(var i = 0; i < recordUserList.length; i++) {
      const recordUserId = recordUserList[i]
      if(userId === recordUserId) {
        recordUserList.splice(i, 1)
        tempConfig.record_users = recordUserList
        break
      }
    }
  }
  else if(userEnableRecording && recordUserList.indexOf(userId) <= -1) {
    recordUserList.push(userId)
    tempConfig.record_users = recordUserList
  }
  await config.updateConfig(tempConfig)
}

async function addServer(serverId) {
  var tempConfig = config.getConfig()
  var servers = config.getServers()
  const serverIds = servers.map(server => server.server_id)
  if(serverIds.indexOf(serverId) <= -1) {
    servers.push({
      server_id: serverId,
      channel_id: null,
      music_bot_id: null,
      music_bot_name: null,
      queue_command: null,
      skip_command: null,
      stop_command: null
    })
    tempConfig.servers = servers
    await config.updateConfig(tempConfig)
  }
}

async function removeServer(serverId) {
  var tempConfig = config.getConfig()
  var servers = config.getServers()
  const serverIds = servers.map(server => server.server_id)
  for(var i = 0; i < serverIds.length; i++) {
    if(serverIds[i] === serverId) {
      servers.splice(i, 1)
      tempConfig.servers = servers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function setTextChannel(serverId, channelId) {
  var tempConfig = config.getConfig()
  const servers = config.getServers()
  for(var i = 0; i < servers.length; i++) {
    const server = servers[i]
    if(server.server_id === serverId) {
      server.channel_id = channelId
      tempConfig.servers = servers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function setMusicBot(serverId, botId) {
  var tempConfig = config.getConfig()
  const servers = config.getServers()
  for(var i = 0; i < servers.length; i++) {
    const server = servers[i]
    if(server.server_id === serverId) {
      server.music_bot_id = botId
      tempConfig.servers = servers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function setMusicBotName(serverId, name) {
  var tempConfig = config.getConfig()
  const servers = config.getServers()
  for(var i = 0; i < servers.length; i++) {
    const server = servers[i]
    if(server.server_id === serverId) {
      server.music_bot_name = name
      tempConfig.servers = servers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function setMusicBotPrefix(serverId, prefix) {
  var tempConfig = config.getConfig()
  const servers = config.getServers()
  for(var i = 0; i < servers.length; i++) {
    const server = servers[i]
    if(server.server_id === serverId) {
      server.music_bot_prefix = prefix
      tempConfig.servers = servers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function setQueueCommand(serverId, cmd) {
  var tempConfig = config.getConfig()
  const servers = config.getServers()
  for(var i = 0; i < servers.length; i++) {
    const server = servers[i]
    if(server.server_id === serverId) {
      server.queue_command = cmd
      tempConfig.servers = servers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function setSkipCommand(serverId, cmd) {
  var tempConfig = config.getConfig()
  const servers = config.getServers()
  for(var i = 0; i < servers.length; i++) {
    const server = servers[i]
    if(server.server_id === serverId) {
      server.skip_command = cmd
      tempConfig.servers = servers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function setStopCommand(serverId, cmd) {
  var tempConfig = config.getConfig()
  const servers = config.getServers()
  for(var i = 0; i < servers.length; i++) {
    const server = servers[i]
    if(server.server_id === serverId) {
      server.stop_command = cmd
      tempConfig.servers = servers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function setBannedWord(word) {
  var tempConfig = config.getConfig()
  var bannedWords = config.getBannedWords()
  if(bannedWords.indexOf(word) <= -1) {
    bannedWords.push(word)
    tempConfig.banned_words = bannedWords
    await config.updateConfig(tempConfig)
  }
}

async function setBannedSong(song) {
  var tempConfig = config.getConfig()
  var bannedSongs = config.getBannedSongs()
  if(bannedSongs.indexOf(song) <= -1) {
    bannedSongs.push(song)
    tempConfig.banned_songs = bannedSongs
    await config.updateConfig(tempConfig)
  }
}

async function setAdminUser(userId) {
  var tempConfig = config.getConfig()
  var adminUsers = config.getAdminUsers()
  if(adminUsers.indexOf(userId) <= -1) {
    adminUsers.push(userId)
    tempConfig.admin_users = adminUsers
    await config.updateConfig(tempConfig)
  }
}

async function removeBannedWord(word) {
  var tempConfig = config.getConfig()
  var bannedWords = config.getBannedWords()
  for(var i = 0; i < bannedWords.length; i++) {
    const bannedWord = bannedWords[i]
    if(word === bannedWord) {
      bannedWords.splice(i, 1)
      tempConfig.banned_words = bannedWords
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function removeBannedSong(song) {
  var tempConfig = config.getConfig()
  var bannedSongs = config.getBannedSongs()
  for(var i = 0; i < bannedSongs.length; i++) {
    const bannedSong = bannedSongs[i]
    if(song === bannedSong) {
      bannedSongs.splice(i, 1)
      tempConfig.banned_songs = bannedSongs
      await config.updateConfig(tempConfig)
      break
    }
  }
}

async function removeAdminUser(userId) {
  var tempConfig = config.getConfig()
  const adminUsers = config.getAdminUsers()
  for(var i = 0; i < adminUsers.length; i++) {
    const adminUser = adminUsers[i]
    if(userId === adminUser) {
      adminUsers.splice(i, 1)
      tempConfig.admin_users = adminUsers
      await config.updateConfig(tempConfig)
      break
    }
  }
}

// ===================PRIVATE=================== //
function onSpeakingCallback() {
  const voiceConnections = Global.discordClient.voice.connections.map((connection => connection))
  for(voiceConnection of voiceConnections) {
    if(Global.listeningVoiceConnections.indexOf(voiceConnection) <= -1) {
      Global.listeningVoiceConnections.push(voiceConnection)
      if(vc0 === null) {
        vc0 = voiceConnection
        vc0.on('speaking', (user, speaking) => {
          if (!shouldStreamUserVoice(speaking, user)) {
            return
          }
          printLog(`Listening to ${user.username}`)
          const receiver = vc0.receiver
          // this creates a 16-bit signed PCM, stereo 48KHz PCM stream.
          const audioStream = receiver.createStream(user, { mode: 'pcm' })
          voiceRecognitionStream(audioStream, vc0, user)
        })
      }
      else if(vc1 === null) {
        vc1 = voiceConnection
        vc1.on('speaking', (user, speaking) => {
          if (!shouldStreamUserVoice(speaking, user)) {
            return
          }
          printLog(`Listening to ${user.username}`)
          const receiver = vc1.receiver
          // this creates a 16-bit signed PCM, stereo 48KHz PCM stream.
          const audioStream = receiver.createStream(user, { mode: 'pcm' })
          voiceRecognitionStream(audioStream, vc1, user)
        })
      }
      else if(vc2 === null) {
        vc2 = voiceConnection
        vc2.on('speaking', (user, speaking) => {
          if (!shouldStreamUserVoice(speaking, user)) {
            return
          }
          printLog(`Listening to ${user.username}`)
          const receiver = vc2.receiver
          // this creates a 16-bit signed PCM, stereo 48KHz PCM stream.
          const audioStream = receiver.createStream(user, { mode: 'pcm' })
          voiceRecognitionStream(audioStream, vc2, user)
        })
      }
      else if(vc3 === null) {
        vc3 = voiceConnection
        vc3.on('speaking', (user, speaking) => {
          if (!shouldStreamUserVoice(speaking, user)) {
            return
          }
          printLog(`Listening to ${user.username}`)
          const receiver = vc3.receiver
          // this creates a 16-bit signed PCM, stereo 48KHz PCM stream.
          const audioStream = receiver.createStream(user, { mode: 'pcm' })
          voiceRecognitionStream(audioStream, vc3, user)
        })
      }
      else if(vc4 === null) {
        vc4 = voiceConnection
        vc4.on('speaking', (user, speaking) => {
          if (!shouldStreamUserVoice(speaking, user)) {
            return
          }
          printLog(`Listening to ${user.username}`)
          const receiver = vc4.receiver
          // this creates a 16-bit signed PCM, stereo 48KHz PCM stream.
          const audioStream = receiver.createStream(user, { mode: 'pcm' })
          voiceRecognitionStream(audioStream, vc4, user)
        })
      }
    }
  }
}

function shouldStreamUserVoice(speaking, user) {
  const botIdsInAllServers = config.getAllBotIds()
  const rootSpeechListenerEnabled = config.getSpeechCaptureEnabledStatus()
  const usersToListenTo = config.getUsersWhoUseVoiceEngine()
  // If user is not speaking, user speaking is a music bot, didn't opt-in to voice listening by cloud engines, bot owner set cloud listening to 'Off', or audio is being broadcasted, DON'T STREAM USER'S AUDIO
  return !(!userIsSpeaking(speaking.has(1)) || botIdsInAllServers.indexOf(user.id) > -1 || usersToListenTo.indexOf(user.id) <= -1 || !rootSpeechListenerEnabled || broadcastIsPlaying)
}

// speaking.has(<value>) is too confusing
function userIsSpeaking(isSpeaking) {
  return isSpeaking
}

Event.on('Discord', async (args) => {
  if(args.length <= 0) { return }
  const functionName = args[0]
  switch(functionName) {
    case 'sendMessageToServers':
      const msg0 = args[1]
      if(typeof msg0 !== 'undefined' && typeof msg0 === 'string') {
        await sendMessageToServers(msg0)
      }
      break
    case 'sendMessageToBotOwner':
      const msg1 = args[1]
      const opts = args[2]
      await sendMessageToBotOwner(msg1, opts)
      break
    case 'sendMessageToUser':
      const userId0 = args[1]
      const msg2 = args[2]
      await sendMessageToUser(userId0, msg2)
      break
    case 'joinChannel':
      const voiceChannel0 = args[1]
      joinChannel(voiceChannel0)
      break
    case 'leaveChannel':
      const voiceChannel1 = args[1]
      leaveChannel(voiceChannel1)
      break
    case 'punishUser':
      const guildMember = args[1]
      await punishUser(guildMember)
      break
    case 'setVoiceListeningForUser':
      const ignoreEnabled3 = args[1]
      const userId3 = args[2]
      await setVoiceListeningForUser(ignoreEnabled3, userId3)
      break
    case 'setVoiceRecorderForUser':
      const ignoreEnabled4 = args[1]
      const userId4 = args[2]
      await setVoiceRecorderForUser(ignoreEnabled4, userId4)
      break
    case 'setTextChannel':
      const serverId0 = args[1]
      const channelId0 = args[2]
      await setTextChannel(serverId0, channelId0)
      break
    case 'setMusicBot':
      const serverId1 = args[1]
      const botId0 = args[2]
      await setMusicBot(serverId1, botId0)
      break
    case 'setMusicBotName':
      const serverId2 = args[1]
      const botName = args[2]
      await setMusicBotName(serverId2, botName)
      break
    case 'setMusicBotPrefix':
      const serverId3 = args[1]
      const prefix = args[2]
      await setMusicBotPrefix(serverId3, prefix)
      break
    case 'setQueueCommand':
      const serverId4 = args[1]
      const cmd0 = args[2]
      await setQueueCommand(serverId4, cmd0)
      break
    case 'setSkipCommand':
      const serverId5 = args[1]
      const cmd1 = args[2]
      await setSkipCommand(serverId5, cmd1)
      break
    case 'setStopCommand':
      const serverId6 = args[1]
      const cmd2 = args[2]
      await setStopCommand(serverId6, cmd2)
      break
    case 'setBannedWord':
      const word0 = args[1]
      await setBannedWord(word0)
      break
    case 'setBannedSong':
      const song0 = args[1]
      await setBannedSong(song0)
      break
    case 'setAdminUser':
      const userId5 = args[1]
      await setAdminUser(userId5)
      break
    case 'removeBannedWord':
      const word1 = args[1]
      await removeBannedWord(word1)
      break
    case 'removeBannedSong':
      const song1 = args[1]
      await removeBannedSong(song1)
      break
    case 'removeAdminUser':
      const userId6 = args[1]
      await removeAdminUser(userId6)
      break
  }
})

module.exports = {
  joinChannel,
  leaveChannel,
  sendMessageToServers,
  sendMessageToBotOwner,
  setVoiceListeningForUser,
  setVoiceRecorderForUser,
  setTextChannel,
  setBannedWord,
  setBannedSong,
  setAdminUser,
  removeBannedWord,
  removeBannedSong,
  removeAdminUser,
}