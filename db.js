const { Pool } = require('pg')
const fs = require('fs')
const path = require('path')
const homedir = require('os').homedir()
const dateFormat = require('dateformat')
const Event = require('./Events').eventBus

const TABLE_NAME = 'soundbites'
const LIMIT = 'LIMIT 50'
const CREATE_TABLE = `CREATE TABLE ${TABLE_NAME} ( id serial PRIMARY KEY, userId VARCHAR (24), username VARCHAR (32) UNIQUE NOT NULL, file_name VARCHAR (128) NOT NULL, file_path VARCHAR (255) UNIQUE NOT NULL, created_on TIMESTAMP NOT NULL );`
const INSERT_ROW = `INSERT INTO ${TABLE_NAME} (user_id, username, file_name, file_path, created_on) VALUES($1, $2, $3, $4, $5) RETURNING *` // use '*' to return whole row
const DELETE_TABLE_CONTENTS = `TRUNCATE ${TABLE_NAME}`
const GET_ALL_FILES = `SELECT * FROM ${TABLE_NAME} ${LIMIT}`
const GET_FILES_BY_ID = `SELECT * FROM ${TABLE_NAME} ORDER BY id DESC OFFSET $1 ROWS ${LIMIT}`
const SEARCH_ALL_DB_FIELDS = `SELECT * FROM ${TABLE_NAME} WHERE LOWER(username) LIKE LOWER($1) OR LOWER(file_name) LIKE LOWER($1) OR LOWER(file_path) LIKE LOWER($1) ORDER BY id DESC OFFSET $2 ${LIMIT}`
const SEARCH_USERNAME_FIELD = `SELECT * FROM ${TABLE_NAME} WHERE LOWER(username) LIKE LOWER($1) ORDER BY id DESC OFFSET $2 ${LIMIT}`
const BEGIN = 'BEGIN'
const COMMIT = 'COMMIT'
const ROLLBACK = 'ROLLBACK'

const soundbitesFolderName = "Soundbites"
const monthDayYearHourMinSecTimeZoneFormat = "mm/dd/yyyy hh:MM:ss TT Z"

const pool = new Pool({
  user: process.env.POSTGRES_USERNAME,
  password: process.env.POSTGRES_PASSWORD,
  host: "localhost",
  database: "dumbo",
  port: "5432"
})

async function isTableEmpty() {
  const client = await pool.connect()
  let isEmpty;
  try {
    await client.query(BEGIN)
    const response = await client.query(GET_ALL_FILES)
    isEmpty = response.rowCount <= 0
    await client.query(COMMIT)
  } catch (error) {
    await client.query(ROLLBACK)
    throw error

  } finally {
    client.release()
    return isEmpty
  }
}

async function addEverySoundbite() {
  // note: we don't try/catch this because if connecting throws an exception
  // we don't need to dispose of the client (it will be undefined)
  const client = await pool.connect()
  try {
    await client.query(BEGIN)
    const soundbiteFolder = path.join(homedir, soundbitesFolderName)
    await digThroughFolder(client, soundbiteFolder)
    await client.query(COMMIT)
  } catch (error) {
    await client.query(ROLLBACK)
    throw error
  } finally {
    client.release()
  }
}

async function addFile(userId, userName, fileName, filePath, createdDate) {
  const client = await pool.connect()
  try {
    await client.query(BEGIN)
    const insertValues = [userId, userName, fileName, filePath, createdDate]
    const response = await client.query(INSERT_ROW, insertValues)
    const jsonObj = { data: response.rows }
    Event.emit('WebSocket', ['newSoundbite', response.rows])
    await client.query(COMMIT)
  } catch (error) {
    await client.query(ROLLBACK)
    throw error
  } finally {
    client.release()
  }
}

async function clearTable() {
  const client = await pool.connect()
  try {
    await client.query(BEGIN)
    await client.query(DELETE_TABLE_CONTENTS)
    await client.query(COMMIT)
  } catch (error) {
    await client.query(ROLLBACK)
    throw error
  } finally {
    client.release()
  }
}

async function getAllFiles() {
  const client = await pool.connect()
  let response = []
  try {
    await client.query(BEGIN)
    response = await client.query(GET_ALL_FILES)
    await client.query(COMMIT)
  } catch (error) {
    await client.query(ROLLBACK)
    throw error
  } finally {
    client.release()
    return response.rows
  }
}

async function getFilesById(offset) {
  const client = await pool.connect()
  let response = []
  try {
    await client.query(BEGIN)
    response = await client.query(GET_FILES_BY_ID, [offset])
    await client.query(COMMIT)
  } catch (error) {
    await client.query(ROLLBACK)
    throw error
  } finally {
    client.release()
    return response.rows
  }
}

async function search(text, offset) {
  const client = await pool.connect()
  let response = []
  try {
    await client.query(BEGIN)
    const regex = new RegExp(/[^\s]+/g,"g")
    const search = text.replace(regex, `%$&%`)
    response = await client.query(SEARCH_ALL_DB_FIELDS, [search, offset])
    await client.query(COMMIT)
  } catch (error) {
    await client.query(ROLLBACK)
    throw error
  } finally {
    client.release()
    return response.rows
  }
}

async function getUsernameCategory(group, offset) {
  const client = await pool.connect()
  let response = []
  try {
    await client.query(BEGIN)
    const regex = new RegExp(/[^\s]+/g,"g")
    const search = group.replace(regex, `%$&%`)
    response = await client.query(SEARCH_USERNAME_FIELD, [search, offset])
    await client.query(COMMIT)
  } catch (error) {
    await client.query(ROLLBACK)
    throw error
  } finally {
    client.release()
    return response.rows
  }
}

async function refresh() {
  var response
  const client = await pool.connect()
  try {
    await clearTable()
    await addEverySoundbite()
    response = "Database Refreshed"
  } catch (error) {
    response = "An Error occurred while trying to refresh the database"
    throw error
  } finally {
    client.release()
    return response
  }
}

async function digThroughFolder(client, parentDirectory, parentUserId, parentUsername) {
  const folders = await fs.promises.readdir(parentDirectory)
  for (folder of folders) {
    const folderPath = path.join(parentDirectory, folder)
    if(fs.lstatSync(folderPath).isDirectory() && !folder.startsWith('.')) {
      const userIdName = folder.split('-')
      let userId;
      let username;
      let isnum = /^\d+$/.test(userIdName[0])
      if(isnum) {
        userId = userIdName[0]
        username = userIdName[1]
      }
      else {
        userId = null
        username = userIdName[0]
      }
      await digThroughFolder(client, folderPath, userId, username)
    }
    else if(fs.lstatSync(folderPath).isFile() && !folder.startsWith('.') && folder !== 'Icon?' && folder !== 'Icon\r') {
      const userId = parentUserId
      const username = parentUsername
      const fileExtensionIndex = folder.lastIndexOf('.')
      const fileName = folder.substr(0, fileExtensionIndex)
      const fileStat = fs.statSync(folderPath)
      const createdDate = dateFormat(fileStat.ctime, monthDayYearHourMinSecTimeZoneFormat)
      const insertValues = [userId, username, fileName, folderPath, createdDate]
      const response = await client.query(INSERT_ROW, insertValues)
    }
  }
}

module.exports = {
  isTableEmpty,
  addEverySoundbite,
  addFile,
  search,
  getUsernameCategory,
  refresh
}