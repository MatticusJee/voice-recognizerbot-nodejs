const { exec } = require('child_process')
const dateFormat = require('dateformat')
const disk = require('diskusage')
const Event = require('./Events').eventBus

var availableSpaceInGigabytes = 0
var autoDisableEncoder = false
const monthDayYearHourMinSecFormat = "mm/dd/yyyy hh:MM:ss TT"
const monthYearFormat = 'mm_yyyy'

function getGitBranchName() {
  return new Promise((resolve, reject) => {
    exec('git rev-parse --abbrev-ref HEAD', (err, stdout, stderr) => {
      if (err) {
        reject(`GIT_STATUS_ERROR: ${err}`)
      }
      else if(typeof stdout === 'string') {
        const branchName = stdout.trim()
        printLog(`Current branch: ${branchName}`)
        resolve(branchName)
      }
    })
  })
}

function backupDataFolder() {
  const date = dateFormat(Date(), monthYearFormat)
  exec(`cp -R data ../dumbo_data_backup_${date}`, (error, stdout, stderr) => {
    if(error) {
      printError(`BACKUP_ERROR: ${error}`)
    }
    else {
      printLog(`Backed up data folder (${date})`)
    }
  })
}

function getDiskInfo() {
  // info.available: Disk space available to the current user (i.e. Linux reserves 5% for root)
  // info.free: Disk space physically free
  // info.total: Total disk space (free + used)\
  disk.check("/").then((info) => {
    const bytesInMegabyte = 1024 * 1024 // == 1,048,576
    const megaBytesInGigabyte = 1024
    const availableMegabytes = info.available / bytesInMegabyte
    availableSpaceInGigabytes = Math.round(100 * (availableMegabytes / megaBytesInGigabyte)) / 100 // rounds to nearest 100th
    printLog("Amount of Available Space Remaining (In Megabytes): " + availableMegabytes)
    printLog("Amount of Available Space Remaining (In Gigabytes): " + availableSpaceInGigabytes)
    if(availableMegabytes <= megaBytesInGigabyte / 2) { // Half a Gigabyte
      printLog("WARNING: NOT ENOUGH STORAGE TO SAVE RECORDINGS. PLEASE FREE UP SOME SPACE")
      if(autoDisableEncoder === false) {
        Event.emit('Discord', ['sendMessageToServers', 'Recorder Mode is set to **ON**'])
      }
      autoDisableEncoder = true
    }
    else {
      if(autoDisableEncoder === true) {
        Event.emit('Discord', ['sendMessageToServers', 'Recorder Mode is set to **OFF**'])
      }
      autoDisableEncoder = false
    }
  }).catch(function(error) {
    printError(`DISK_INFO_ERROR: ${error}`)
  })
}

function clearLogFile() {
  exec(`echo "" > ../bot_tom_bitch_logs.txt`, (error, stdout, stderr) => {
    if(error) {
      printError(`CLEAR_LOG_FILE_ERROR: ${error}`)
    }
    else {
      printLog(`CLEARED LOG FILE!`)
    }
  })
}

function getAvailableDiskSpace() {
  return availableSpaceInGigabytes
}

function isAutoEncoderDisabled() {
  return autoDisableEncoder
}

function printLog(msg) {
  const date = dateFormat(Date(), monthDayYearHourMinSecFormat)
  console.log(`${date} // ${msg}`)
}

function printError(msg) {
  const date = dateFormat(Date(), monthDayYearHourMinSecFormat)
  const msgFormat = `${date} // ${msg}`
  console.log(msgFormat)
  Event.emit('Discord', ['sendMessageToBotOwner', msgFormat])
}

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

Event.on('System', (args) => {
  if(args.length <= 0) { return }
  const functionName = args[0]
  switch(functionName) {
    case 'getDiskInfo':
      getDiskInfo()
      break
    case 'clearLogFile':
      clearLogFile()
      break
  }
})

module.exports = {
  getGitBranchName,
  backupDataFolder,
  getDiskInfo,
  getAvailableDiskSpace,
  isAutoEncoderDisabled,
  printLog,
  printError,
  sleep
}