const db = require('./db')

;(async () => {
  const isTableEmpty = await db.isTableEmpty()
  if(typeof isTableEmpty !== 'undefined') {
    if(isTableEmpty) {
      await db.addEverySoundbite()
    }
    require('./cron').pianoManListener()
    require('./cron').oneMoreSaturdayNightListener()
    require('./discord')
    require('./websocket')
  }
  else {
    process.exit(-1)
  }
})().catch(e => console.error(e.stack))


// process.argv.forEach(function (val, index, array) {
//   // printLog(index + ': ' + val);
//   // process.exit(5551)
// })

// process.on('exit', function(code) {
//   return code
// })

// Extra code that could be useful later
// 1: discordClient.user.setStatus('invisible').then((presence) => { printLog(presence) }).catch(console.error) // other types: 'online' | 'idle' | 'dnd' | 'invisible