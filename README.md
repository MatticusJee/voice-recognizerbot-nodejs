## Voice-RecognizerBot-NodeJS (aka. Dumbo)

Dumbo is a Discord bot full of fun features that are just as enjoyable for the communities that welcome the bot as
it is for the one who created it for himself!

___

Dumbo has the following features:

- Auto summon to any discord channel the bot is allowed to join and welcome the voice channel with an opening message
- Trigger audio tracks based on who said certain phrases or if a specific person was summoned to the voice channel
- Play Billy Joel's Piano Man at a very specific Saturday night time (8:59:29)
- Soundboard: Connect to an iPad via a websocket that would allow one to play sounds on cmd
- Capture audio from users (powered by Google/Microsoft/IBM cloud voice engines with user permission) that could be a 
source of comedic relief with the Soundboard
- Listen for user cmds via voice to request music (which is done in conjunction with a fork of the Red-Discord bot project)

___

What is needed to improve the project:

- A proper architecture
- Unit Tests
- More documented code

___

How to run bot:
- To be continued...