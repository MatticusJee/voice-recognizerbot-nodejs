const dateFormat = require('dateformat')
const pm2 = require('pm2')
const config = require('./config')
const Global = require('./Global')
// const { speechEngine } = require('./speech')
const { getUserSpeechTimeOfMonth, getTotalSpeechTimeOfMonth } = require('./speechTracker')
const { getAvailableDiskSpace, printLog, printError } = require('./system')
const Event = require('./Events').eventBus

const botOwner = config.getBotOwner()

const appName = "index"
const monthYearFormat = 'mm_yyyy'

const join = 'join'
const leave = 'leave'
const restart = 'restart'
const voiceListener = 'voice_listener'
const voiceRecording = 'voice_recording'
const set = 'set'
const remove = 'remove'
const report = 'report'
const owner = 'owner'
const info = 'info'
const help = 'help'

const cmdList = {
  join: "Bot joins voice channel user is in",
  leave: "Bot leaves whatever voice channel it's",
  restart: "Restart bot",
  voice_listener: "Toggles cloud listening for user",
  voice_recording: "Toggles audio recording for user",
  set: "Set a config set for the bot/server",
  remove: "Remove a config set for the bot/server",
  report: "If the bot is something suspicious, report it to the bot owner",
  owner: "Commands only the bot owner can execute",
  info: "Returns info about the bot (name, owner, amt of available space, how long it's been running, etc)",
  help: "List available commmands"
}

const voiceListeningList = {
  enable: "Enables user voice to be transcribed by voice engines powered by the cloud. Please speak responsibly.",
  disable: "Disables cloud voice listening for the user. User will no longer be able to communicate to the bot",
  help: "List available voice listening commands"
}

const voiceRecordingList = {
  enable: "Enables voice recording. Recordings will be stored in the server",
  disable: "Disables voice recording. Recordings will no longer be stored in the server",
  help: "List available voice recording commands"
}

const setList = {
  admin_user: "Set an admin user for the server (BOT-ADMIN ONLY). This opens up a set a new commands for that user. This can be applied to more than one user",
  text_channel: "Set the text_channel where the bot will do most of the messaging, particularly for music commands",
  music_bot: "Tag the music bot that will be handling music requests",
  music_bot_name: "(Optional, WIP) Add a name that you call your music bot. (It should be a name that the bot can translate easily and it should be one word)",
  music_bot_prefix: "Set the prefix that your music bot looks for when listening for commands. (i.e. '!')",
  queue_cmd: "Set the queue command that your music bot looks for when playing audio",
  skip_cmd: "Set the skip command that your music bot looks for when skipping audio",
  stop_cmd: "Set the stop command that your music bot looks for when stopping audio",
  banned_word: "Set a word you want to label 'bad' and the bot will punish any user who says it",
  help: "List available set commands"
}

const removeList = {
  admin_user: "Removes an admin user for the server (BOT-ADMIN ONLY). This can be applied to more than one user",
  banned_word: "Remove banned word",
  help: "List available remove commands"
}

const ownerList = {
  override: "Overrides a user's preferences",
  logs: "Gets a log of the most recent bot activities",
  clear_logs: "Clears Log File",
  banned_song: "Adds/Removes a phrase or link to be banned"
}

// ===================PUBLIC=================== //

async function runServerCommand(cmd, args, user, channel) {
  switch(cmd) {
    case join:
      joinCommand(user, channel)
      break
    case leave:
      leaveCommand(user, channel)
      break
    case restart:
      restartCommand(user, channel)
      break
    case voiceListener:
      voiceListeningForUser(args, user, channel)
      break
    case voiceRecording:
      voiceRecordingForUser(args, user, channel)
      break
    case set:
      setCommand(args, user, channel)
      break
    case remove:
      removeCommand(args, user, channel)
      break
    case report:
      reportCommand(args, user, channel)
      break
    case owner:
      await ownerCommand(args, user, channel)
      break
    case info:
      infoCommand(user, channel)
      break
    case help:
      helpCommand(channel, 'all', cmdList)
      break
    default:
      logActivity('Invalid Command', channel)
      return
  }
}

// Just send recieved messages/media to bot owner for review
function handleDmMessage(user, msg, attachmentCollection) {
  const attachments = attachmentCollection.map((attachment => attachment.proxyURL))
  if(attachments.length > 0) {
    for(attachment of attachments) {
      if(typeof msg === 'undefined' || msg === '') {
        Event.emit('Discord', ['sendMessageToBotOwner', `**Message from ${user.tag}:**\n${attachment}`])
      }
      else {
        Event.emit('Discord', ['sendMessageToBotOwner', `**Message from ${user.tag}:**\n${msg}\n${attachment}`])
      }
    }
  }
  else {
    Event.emit('Discord', ['sendMessageToBotOwner', `**Message from ${user.tag}:**\n${msg}`])
  }
  user.send('Thank you for your message! The owner of the bot will review the contents of what you just sent')
}

// ===================PRIVATE=================== //

// Check if user is in channel. If so, join the channel
function joinCommand(user, msgChannel) {
  const msgServerId = msgChannel.guild.id
  const server = Global.discordClient.guilds.cache.get(msgServerId)
  const voiceChannels = Global.getListOfAvailableVoiceChannelsInServer(server)
  for(voiceChannel of voiceChannels) {
    const membersInChannel = voiceChannel.members
    for(const [memberId, member] of membersInChannel) {
      if(memberId === user.id) {
        Event.emit('Discord', ['joinChannel', voiceChannel])
        break
      }
    }
  }
}

function leaveCommand(user, msgChannel) {
  const msgServerId = msgChannel.guild.id
  const server = Global.discordClient.guilds.cache.get(msgServerId)
  const voiceChannels = Global.getListOfAvailableVoiceChannelsInServer(server)
  for(voiceChannel of voiceChannels) {
    const membersInChannel = voiceChannel.members
    for(const [memberId, member] of membersInChannel) {
      if(memberId === user.id) {
        Event.emit('Discord', ['leaveChannel', voiceChannel])
        break
      }
    }
  }
}

// Restart bot
function restartCommand(user, channel) {
  const adminUsers = config.getAdminUsers()
  if(typeof user !== 'undefined' && adminUsers.indexOf(user.id) > -1) {
    printLog("Restarting!")
    if (typeof channel !== 'undefined') {
      logActivity(`Restarting!`, channel)
    }
    pm2.restart(appName)
  }
}

function voiceListeningForUser(args, user, channel) {
  if(args.length > 0) {
    switch(args[0]) {
      case 'enable':
        Event.emit('Discord', ['setVoiceListeningForUser', args[0], user.id])
        logActivity(`Cloud Voice Listening ENABLED for <@!${user.id}>\nWARNING: PLEASE USE THIS RESPONSIBLY!\nThere's only so much free voice buffer I can process and send over to the Cloud Voice Engines.\nIf you're not gonna talk to me or make use of the audio-triggers, please opt-out of this.\nIf you are found to be inconsiderate for your lack of use for this feature, you can be banned by the bot owner.\nThank you for your understanding!`, channel)
        break
      case 'disable':
        Event.emit('Discord', ['setVoiceListeningForUser', args[0], user.id])
        logActivity(`Cloud Voice Listening DISABLED for <@!${user.id}>`, channel)
        break
      default:
        logActivity(`Invalid argument`)
        return
    }
  }
  else {
    helpCommand(channel, 'Voice Listening', voiceListeningList)
  }
}

function voiceRecordingForUser(args, user, channel) {
  if(args.length > 0) {
    switch(args[0]) {
      case 'enable':
        Event.emit('Discord', ['setVoiceRecorderForUser', args[0], user.id])
        logActivity(`Voice Recording ENABLED for <@!${user.id}>`, channel)
        break
      case 'disable':
        Event.emit('Discord', ['setVoiceRecorderForUser', args[0], user.id])
        logActivity(`Voice Recording DISABLED for <@!${user.id}>`, channel)
        break
      default:
        logActivity(`Invalid argument`)
        return
    }
  }
  else {
    helpCommand(channel, 'Voice Recording', voiceRecordingList)
  }
}

function setCommand(args, user, channel) {
  const adminUsers = config.getAdminUsers()
  if(adminUsers.indexOf(user.id) <= -1) {
    logActivity('You must be a bot admin user to execute this command', channel)
    return
  }
  else if(args.length <= 0) {
    helpCommand(channel, 'Set', setList)
    return
  }
  switch(args[0]) {
    case 'admin_user':
      if(args.length > 1) {
        const taggedUser = args[1]
        if(isValidUser(taggedUser)) {
          const userId = taggedUser.match(/\w+/g)[0]
          Event.emit('Discord', ['setAdminUser', userId])
          logActivity(`${taggedUser} is now an admin!`, channel)
        }
        else {
          logActivity('Not a valid user')
        }
        break
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'text_channel':
      if(args.length > 1) {
        const serverId = channel.guild.id
        const taggedChannel = args[1]
        if(isValidTextChannel(taggedChannel)) {
          const channelId = taggedChannel.match(/\w+/g)[0]
          Event.emit('Discord', ['setTextChannel', serverId, channelId])
          logActivity(`text channel is set to ${taggedChannel}!`, channel)
        }
        else {
          logActivity('Not a valid channel')
        }
        break
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'music_bot':
      if(args.length > 1) {
        const serverId = channel.guild.id
        const taggedMusicBot = args[1]
        if(isValidUser(taggedMusicBot)) {
          const botId = taggedMusicBot.match(/\w+/g)[0]
          Event.emit('Discord', ['setMusicBot', serverId, botId])
          logActivity(`${taggedMusicBot} is now set as the music bot!`, channel)
        }
        else {
          logActivity('Not a valid user')
        }
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'music_bot_name':
      if(args.length > 1) {
        const serverId = channel.guild.id
        const botName = args[1].toLowerCase()
        Event.emit('Discord', ['setMusicBotName', serverId, botName])
        logActivity(`***${botName}*** is now set as the music bot name!`, channel)
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'music_bot_prefix':
      if(args.length > 1) {
        const serverId = channel.guild.id
        const prefix = args[1]
        Event.emit('Discord', ['setMusicBotPrefix', serverId, prefix])
        logActivity(`***${prefix}*** is now set as the music bot prefix!`, channel)
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'queue_cmd':
      if(args.length > 1) {
        const serverId = channel.guild.id
        const cmd = args[1]
        Event.emit('Discord', ['setQueueCommand', serverId, cmd])
        logActivity(`***${cmd}*** is now set as the music bot queue command!`, channel)
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'skip_cmd':
      if(args.length > 1) {
        const serverId = channel.guild.id
        const cmd = args[1]
        Event.emit('Discord', ['setSkipCommand', serverId, cmd])
        logActivity(`***${cmd}*** is now set as the music bot skip command!`, channel)
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'stop_cmd':
      if(args.length > 1) {
        const serverId = channel.guild.id
        const cmd = args[1]
        Event.emit('Discord', ['setStopCommand', serverId, cmd])
        logActivity(`***${cmd}*** is now set as the music bot stop command!`, channel)
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'banned_word':
      if(args.length > 1) {
        const word = args.slice(1).join(' ')
        Event.emit('Discord', ['setBannedWord', word])
        logActivity(`${word} is now banned!`, channel)
        break
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    default:
      logActivity('Invalid argument(s)', channel)
      return
  }
}

function removeCommand(args, user, channel) {
  const adminUsers = config.getAdminUsers()
  if(adminUsers.indexOf(user.id) <= -1) {
    logActivity('You must be a bot admin user to execute this command', channel)
    return
  }
  else if(args.length <= 0) {
    helpCommand(channel, 'Remove', removeList)
    return
  }
  switch(args[0]) {
    case 'banned_word':
      if(args.length > 1) {
        const word = args.slice(1).join(' ')
        Event.emit('Discord', ['removeBannedWord', word])
        logActivity(`${word} is no longer banned!`, channel)
        break
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    case 'admin_user':
      if(args.length > 1) {
        const userId = args[1]
        Event.emit('Discord', ['removeAdminUser', userId])
        logActivity(`${userId} is no longer an admin!`, channel)
        break
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    default:
      logActivity('Invalid argument(s)', channel)
      return
  }
}

function reportCommand(args, user, channel) {
  if(args.length <= 0) {
    logActivity('A message must follow the command phrase', channel)
  }
  else {
    const msg = args.join(' ')
    Event.emit('Discord', ['sendMessageToBotOwner', `**REPORT BY ${user.tag}:**\n${msg}`])
    logActivity('Thank you for sending your report! The bot owner will take a look at your report as soon as he/she can', channel)
  }
}

async function ownerCommand(args, user, channel) {
  if(user.id !== botOwner) {
    logActivity('This command can only be executed by the bot owner', channel)
    return
  }
  if(args.length <= 0) {
    helpCommand(channel, 'Owner', ownerList)
    return
  }
  const overrideVsLogs = args[0]
  switch(overrideVsLogs) {
    case 'override':
      if(args[1] === 'recorder') {
        const enableDisable = args[2]
        const userId = args[3].match(/\w+/g)[0]
        Event.emit('Discord', ['setVoiceRecorderForUser', enableDisable, userId])
        logActivity(`Voice-recording changed for user ${args[3]}`, channel)
      }
      else if(args[1] === 'voice_listener') {
        const enableDisable = args[2]
        const userId = args[3].match(/\w+/g)[0]
        Event.emit('Discord', ['setVoiceListeningForUser', enableDisable, userId])
        logActivity(`Voice-Listening changed for user ${args[3]}`, channel)
      }
      else {
        logActivity(`Invalid argument`, channel)
      }
      break
    case 'logs':
      const homedir = require('os').homedir()
      const path = require('path')
      const logFile = { files: [path.join(homedir, 'bot_tom_bitch_logs.txt')] }
      Event.emit('Discord', ['sendMessageToBotOwner', '**LOGS:**', logFile])
      logActivity(`Sending logs to owner`, channel)
      break
    case 'clear_logs':
      Event.emit('System', ['clearLogFile'])
      logActivity(`Clearing Log File`, channel)
      break
    case 'banned_song':
      if(args.length > 1) {
        const enableDisable = args[1]
        const song = args.slice(2).join(' ')
        if(enableDisable === 'add') {
          Event.emit('Discord', ['setBannedSong', song])
          logActivity(`${song} is now banned!`, channel)
        }
        else if(enableDisable === 'remove') {
          Event.emit('Discord', ['removeBannedSong', song])
          logActivity(`${song} is no longer banned!`, channel)
        }
        else {
          logActivity(`Invalid argument`, channel)
        }
        break
      }
      else {
        logActivity('A second argument is required for this request', channel)
      }
      return
    default:
      logActivity('Invalid argument(s)', channel)
      return
  }
}

// Returns info about the bot (name, owner, amt of available space, how long it's been running, etc)
function infoCommand(user, channel) {
  pm2.describe(appName, function(error, processResult) {
    const monthYear = dateFormat(Date(), monthYearFormat)
    const totalUserSpeechTimeForMonth = getUserSpeechTimeOfMonth(monthYear, user.id)
    const totalSpeechTimeForMonth = getTotalSpeechTimeOfMonth(monthYear)
    const server = config.getServer(channel.guild.id)
    const serverInfo = `**Server ID**: ${server.server_id}\n**Music Channel ID**: ${server.channel_id}\n**Bot ID**: ${server.music_bot_id}\n**Bot Name**: ${server.music_bot_name}\n**Bot Prefix**: ${server.music_bot_prefix}\n**Queue Command**: ${server.queue_command}\n**Skip Command**: ${server.skip_command}\n**Stop Command**: ${server.stop_command}`
    const bannedWords = config.getBannedWords()
    const bannedSongs = config.getBannedSongs()
    var info = `>>> **Name**: ${Global.discordClient.user.tag}\n**Owner**: ${user.tag}\n**Available Space**: ${getAvailableDiskSpace()} GB\n**Current Voice Engine**: ${Global.speechEngine}\n**Total Speech Time For <@!${user.id}> (${monthYear})**: ${totalUserSpeechTimeForMonth} ms\n**Total Speech Time (${monthYear})**: ${totalSpeechTimeForMonth} ms\n${serverInfo}\n**Banned Words**: ${bannedWords}\n**Banned Songs**: ${bannedSongs}`
    if(error) {
      info = `${info}\n**Uptime**: N/A`
      printError("PM2 ERROR -1: " + error)
    }
    else if(typeof processResult[0] !== 'undefined') {
      const now = Date.now() // milliseconds

      const startTime = processResult[0].pm2_env.pm_uptime
      var delta = Math.floor((now - startTime) / 1000)
      // calculate (and subtract) whole days
      const days = Math.floor(delta / 86400)
      delta -= days * 86400

      // calculate (and subtract) whole hours
      const hours = Math.floor(delta / 3600) % 24
      delta -= hours * 3600

      // calculate (and subtract) whole minutes
      const minutes = Math.floor(delta / 60) % 60
      delta -= minutes * 60

      // what's left is seconds
      const seconds = delta % 60  // in theory the modulus is not required
      const uptime = "Days: " + days + ", Hours: " + hours + ", Minutes: " + minutes + ", Seconds: " + seconds
      info = `${info}\n**Uptime**: ${uptime}`
    }
    else {
      info = `${info}\n**Uptime**: N/A`
    }
    logActivity(info, channel)
  })
}

function helpCommand(channel, title, list) {
  const keys = Object.keys(list)
  const values = Object.values(list)
  var msg = `>>> List of ${title} commands:\n`
  for(var i = 0; i < keys.length; i++) {
    msg += `**` + keys[i] + `**: ` + values[i] +`\n`
  }
  logActivity(msg, channel)
}

function logActivity(msg, channel) {
  printLog(msg)
  channel.send(msg)
}

function isValidTextChannel(value) {
  return typeof value === 'string' && value.startsWith("<#") && value.endsWith(">");
}

function isValidUser(value) {
  return typeof value === 'string' && value.startsWith("<@") && value.endsWith(">");
}

Event.on('Command', (args) => {
  if(args.length <= 0) { return }
  const functionName = args[0]
  switch(functionName) {
    case 'restartCommand':
      restartCommand()
  }
})

module.exports = {
  runServerCommand,
  handleDmMessage
}