const pm2 = require('pm2')

pm2.connect(function(error) {
    if(error) {
        console.error(error)
        process.exit(2)
    }

    pm2.list((error, list) => {
        // console.log(error, list)
    })

    // pm2.stop('index.js', (error, process) => {

    // })

    // pm2.restart('index.js', (error, process) => {

    // })
    const homedir = require('os').homedir()
    const path = require('path')

    pm2.start({
        script: 'index.js',       // Script to be run
        restart_delay: "85000",
        exec_mode: 'fork',        // Allows your app to be clustered
        instances: 1,                // Optional: Scales your app by 4
        watch: ['index.js', 'startBot.sh'],
        output: path.join(homedir, 'bot_tom_bitch_logs.txt'),
        error: path.join(homedir, 'bot_tom_bitch_logs.txt'),
        ignore_watch : ['node_modules', 'data'],
        args: process.argv.slice(2)
    }, function(err, apps) {
        if (err) {
            pm2.disconnect();   // Disconnects from PM2
            throw err
        }
    })
})