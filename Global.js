const Discord = require('discord.js')

const discordClient = new Discord.Client()
var availableGuilds = []
var listeningVoiceConnections = []
var guildMembers = [] // Collection [Map] (userId => GuildMember) object
var musicBotIsSpeaking = false
var speechEngine // 'Microsoft' || 'IBM Watson' || 'Google' ||  'Vosk' || 'N/A'

function fetchChannel(channelId) {
  return new Promise((resolve, reject) => {
    discordClient.channels.fetch(channelId, true).then((channel) => {
      resolve(channel)
    }).catch((error) => {
      reject(`FETCH_CHANNEL_ERROR: ${error}`)
    })
  })
}

function getListOfAvailableVoiceChannelsInServer(guild) {
  return guild.channels.cache.filter(channel => channel.type === 'voice').filter((vc) => { return vc.id !== vc.guild.afkChannelID }).map((vc => vc))
}

function getListOfAvailableTextChannelsInServer(guild) {
  return guild.channels.cache.filter(channel => channel.type === 'text').map((channel => channel))
}

module.exports = {
  discordClient,
  availableGuilds,
  listeningVoiceConnections,
  guildMembers,
  musicBotIsSpeaking,
  speechEngine,
  fetchChannel,
  getListOfAvailableVoiceChannelsInServer,
  getListOfAvailableTextChannelsInServer
}