const fs = require('fs')
const path = require('path')
const { Transform } = require('stream')
const pcm = require('pcm-util')
const toWav = require('audiobuffer-to-wav')
const dateFormat = require('dateformat')
const ytdl = require('ytdl-core')
const homedir = require('os').homedir()
const config = require('./config')
const Global = require('./Global')
const { isAutoEncoderDisabled, printLog, printError, sleep } = require('./system')

const channels = 2 // 1 for mono or 2 for stereo
const sampleRate = 48000 // 48khz
const bitDepth = 16

const minBufferLength = 50 // ~0.5 seconds
const maxBufferLength = 250 // ~5 seconds

var audioQueue = [] // array of AudioSource
var audioIsPlaying = {} // key: serverId, value: isPlaying
var broadcastIsPlaying = false
var streamDispatcher = null
const botVolume = 1.0

const soundbitesFolder = "Soundbites"
const yearMonthDayFormat = "yyyy-mm-dd"
const monthDayYearHourMinSecTimeZoneFormat = "mm/dd/yyyy hh:MM:ss TT Z"

// ===================PUBLIC=================== //
class ConvertTo1ChannelStream extends Transform {
  constructor(source, options) {
    super(options)
  }
  
  _transform(buffer, encoding, callback) {
    callback(null, this.convertBufferTo1Channel(buffer))
  }

  convertBufferTo1Channel(samples) {
    const convertedBuffer = Buffer.alloc(samples.length / 2)
    for(let i = 0; i < convertedBuffer.length / 2; i++) {
      const uint16 = samples.readUInt16LE(i * 4)
      convertedBuffer.writeUInt16LE(uint16, i * 2)
    }
    return convertedBuffer
  }
}

class AudioSource {
  constructor(type, source) {
    this.type = type
    this.source = source
  }
}

function getAudioQueue() {
  return audioQueue
}

function clearQueue() {
  audioQueue = []
}

function pauseQueue() {
  streamDispatcher.pause()
}

function resumeQueue() {
  streamDispatcher.resume()
}

function addAudioSourceToQueue(voiceConnection, source) {
  audioQueue.push(source)
  if(!audioIsPlaying[voiceConnection.channel.guild.id] || typeof audioIsPlaying[voiceConnection.channel.guild.id] === 'undefined') {
    const nextAudioToPlay = audioQueue[0]
    if(nextAudioToPlay.type === 'extension') {
      playAudioFileExtension(voiceConnection, nextAudioToPlay.source)
    }
    else if(nextAudioToPlay.type === 'youtube') {
      playYoutubeAudio(voiceConnection, nextAudioToPlay.source)
    }
  }
}

function generateRandomAudioFile(connection, folder) {
  const directoryPath = path.join(homedir, soundbitesFolder, folder)
  fs.readdir(directoryPath, function (error, files) {
    if(error) {
      return printLog("Unable to scan directory: " + error)
    }
    const file = files[Math.floor(Math.random() * files.length)]
    const result = path.join(directoryPath, file)
    printLog(result)
    playAudioFileExtension(connection, result)
  })
}

function playAudioFileExtension(connection, file) {
  if(broadcastIsPlaying === true) return
  const botId = Global.discordClient.user.id
  for(const [serverId, voiceConnection] of Global.discordClient.voice.connections) {
    if(serverId === connection.channel.guild.id) {
      const bots = voiceConnection.channel.members.filter((member) => { return member.user.bot === true && member.user.id !== botId }).map(bot => bot)
      const dispatcher = voiceConnection.play(file, { volume: botVolume })
      dispatchEventHandler(bots, dispatcher, connection)
    }
  }
}

function playYoutubeAudio(connection, link) {
  if(broadcastIsPlaying === true) return
  const botId = Global.discordClient.user.id
  for(const [serverId, voiceConnection] of Global.discordClient.voice.connections) {
    if(serverId === connection.channel.guild.id) {
      const bots = voiceConnection.channel.members.filter((member) => { return member.user.bot === true && member.user.id !== botId }).map(bot => bot)
      const dispatcher = voiceConnection.play(ytdl(link, { filter: "audioonly"}), { volume: botVolume })
      dispatchEventHandler(bots, dispatcher, connection)
    }
  }
}

function broadcastAudioFileExtension(file) {
  const botId = Global.discordClient.user.id
  const broadcast = Global.discordClient.voice.createBroadcast()
  broadcast.play(file)
  const voiceConnections = Global.discordClient.voice.connections.map((connection => connection))
  for(voiceConnection of voiceConnections) {
    const bots = voiceConnection.channel.members.filter((member) => { return member.user.bot === true && member.user.id !== botId }).map((bot => bot))
    const dispatcher = voiceConnection.play(broadcast)
    audioQueue.unshift(file)
    dispatchBroadcastEventHandler(bots, dispatcher, voiceConnection, broadcast)
  }
}

function dispatchEventHandler(bots, dispatcher, connection) {
  streamDispatcher = dispatcher
  streamDispatcher.on('start', async () => {
    for (bot of bots) {
      try {
        await bot.voice.setMute(true)
      } catch (error) {
        throw error
      }
    }
    audioIsPlaying[connection.channel.guild.id] = true
  })
  streamDispatcher.on('finish', async () => {
    audioQueue.shift() // Removes first value of array
    if(audioQueue.length <= 0) {
      for(bot of bots) {
        try {
          await bot.voice.setMute(false)
        } catch (error) {
          throw error
        }
      }
      audioIsPlaying[connection.channel.guild.id] = false
    }
    else {
      const nextAudioToPlay = audioQueue[0]
      if(nextAudioToPlay.type === 'extension') {
        playAudioFileExtension(connection, nextAudioToPlay.source)
      }
      else if(nextAudioToPlay.type === 'youtube') {
        playYoutubeAudio(connection, nextAudioToPlay.source)
      }
    }
  })
  streamDispatcher.on('error', (error) => {
    printError(`DISPATCHER_ERROR: ${error}`)
  })
}

function dispatchBroadcastEventHandler(bots, dispatcher, connection, broadcast) {
  dispatcher.on('start', async () => {
    for(bot of bots) {
      await bot.voice.setMute(true)
    }
    audioIsPlaying[connection.channel.guild.id] = true
    broadcastIsPlaying = true
  })
  broadcast.dispatcher.on('finish', async () => {
    audioQueue.shift() // Removes first value of array
    if(audioQueue.length <= 0) {
      for(bot of bots) {
        await bot.voice.setMute(false)
      }
      audioIsPlaying[connection.channel.guild.id] = false
    }
    else {
      const nextAudioToPlay = audioQueue[0]
      if(nextAudioToPlay.type === 'extension') {
        playAudioFileExtension(connection, nextAudioToPlay.source)
      }
      else if(nextAudioToPlay.type === 'youtube') {
        playYoutubeAudio(connection, nextAudioToPlay.source)
      }
    }
    broadcastIsPlaying = false
  })
  broadcast.dispatcher.on('error', (error) => {
    printError(`BROADCAST_ERROR: ${error}`)
    broadcastIsPlaying = false
  })
}

function bufferShouldBeEncoded(buffer, userId, transcript, isCmd) {
  const recorderUserList = config.getRecordUsers()
  if(buffer.length < minBufferLength && transcript === 'undefined') {
    return false
  }
  else if(buffer.length >= maxBufferLength) {
    return false
  }
  else if(isCmd) {
    return false
  }
  else if(!recorderUserList.includes(userId)) {
    return false
  }
  else if(isAutoEncoderDisabled()) {
    return false
  }
  return true
}

function encodeAudioData(samples, user, transcript) {
  return new Promise(async (resolve) => {
    const buffers = Buffer.concat(samples)
    const pcmFormat = { signed: true, bitDepth: 16, channels: 2, sampleRate: 48000 }
    const pcmToAudioBuffer = pcm.toAudioBuffer(buffers, pcmFormat);
    const wavArrayBuffer = toWav(pcmToAudioBuffer);
    const chunk = new Uint8Array(wavArrayBuffer);
    const date = dateFormat(Date(), yearMonthDayFormat)
    const dateFolder = `${homedir}/${soundbitesFolder}/${date}`
    createFolder(dateFolder)
    const directoryPath = `${dateFolder}/${user.id}-${user.username}`
    createFolder(directoryPath)
    const { fileName, filePath } = wavFile(transcript, `${directoryPath}/${transcript}`)
    fs.writeFile(filePath, new Buffer.from(chunk), function (err) {})
    await sleep(1000)
    const fileStat = fs.statSync(filePath)
    const createdDate = dateFormat(fileStat.ctime, monthDayYearHourMinSecTimeZoneFormat)
    resolve({
      userId: user.id,
      username: user.username,
      fileName: fileName,
      filePath: filePath,
      createdDate: createdDate
    })
  })
}

// ===================PRIVATE=================== //
function createFolder(dir) {
  if (!fs.existsSync(dir)){
    fs.mkdirSync(dir)
  }
}

function wavFile(fileName, filePath, index) {
  var fn
  var fp
  if(index == null) {
    fn = fileName
    fp = filePath
  }
  else {
    fn = `${fileName}${index}`
    fp = `${filePath}${index}`
  }
  if (!fs.existsSync(`${fp}.wav`)) {
    return { fileName: `${fn}.wav`, filePath: `${fp}.wav` }
  }
  else {
    if(index == null) {
      index = -1
    }
    return wavFile(fileName, filePath, index + 1)
  }
}

module.exports = {
  sampleRate,
  bitDepth,
  broadcastIsPlaying,
  ConvertTo1ChannelStream,
  AudioSource,
  getAudioQueue,
  clearQueue,
  pauseQueue,
  resumeQueue,
  addAudioSourceToQueue,
  generateRandomAudioFile,
  playAudioFileExtension,
  broadcastAudioFileExtension,
  bufferShouldBeEncoded,
  encodeAudioData
}