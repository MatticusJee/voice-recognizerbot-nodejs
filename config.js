const fs = require('fs')
const { printLog, printError } = require('./system')
const configPath = './data/config.json'

function getConfig() {
  return JSON.parse(fs.readFileSync(configPath, "utf-8"))
}

function getDiscordToken() {
  const config = getConfig()
  return config.discord_token
}

function getSelfPrefix() {
  const config = getConfig()
  return config.self_prefix
}

function getMusicBotPrefix(serverId) {
  const servers = getServers()
  for(server of servers) {
    if(server.server_id === serverId) {
      return server.music_bot_prefix
    }
  }
  return null
}

function getBotOwner() {
  const config = getConfig()
  return config.owner
}

function getAdminUsers() {
  const config = getConfig()
  return config.admin_users
}

function getServers() {
  const config = getConfig()
  return config.servers
}

function getServer(guildId) {
  const servers = getServers()
  const ids = servers.map( server => server.server_id )
  return servers[ids.indexOf(guildId)]
}

function getTextChannel(serverId) {
  const servers = getServers()
  for(server of servers) {
    if(server.server_id === serverId) {
      return server.channel_id
    }
  }
  return null
}

function getMusicBotId(serverId) {
  const servers = getServers()
  for(server of servers) {
    if(server.server_id === serverId) {
      return server.music_bot_id
    }
  }
  return null
}

function getAllBotIds() {
  return getServers().map(server => server.music_bot_id)
}

function getSpeechCaptureEnabledStatus() {
  const config = getConfig()
  return config.speech_capture_enabled
}

function getUsersWhoUseVoiceEngine() {
  const config = getConfig()
  return config.listen_to_users
}

function getVoiceListeners() {
  const config = getConfig()
  return config.listen_to_users
}

function getRecordUsers() {
  const config = getConfig()
  return config.record_users
}

function getBotAttentionPhrases() {
  const config = getConfig()
  return config.attention_phrases
}

function getSongCommands() {
  const config = getConfig()
  return config.song_commands
}

function getSkipCommands() {
  const config = getConfig()
  return config.skip_commands
}

function getStopCommands() {
  const config = getConfig()
  return config.stop_commands
}

function getCancelCommands() {
  const config = getConfig()
  return config.cancel_commands
}

function getAudioPhrases() {
  const config = getConfig()
  return config.audio_phrases
}

function getBannedWords() {
  const config = getConfig()
  return config.banned_words
}

function getBannedSongs() {
  const config = getConfig()
  return config.banned_songs
}

async function updateConfig(newConfig) {
  try {
    await fs.promises.writeFile(configPath, JSON.stringify(newConfig, null, 2))
    printLog("SUCCESS!")
  } catch (error) {
    printError("UPDATE_CONFIG_ERROR: " + error)
  }
}

module.exports = {
  getConfig,
  getDiscordToken,
  getSelfPrefix,
  getMusicBotPrefix,
  getBotOwner,
  getAdminUsers,
  getServers,
  getServer,
  getTextChannel,
  getMusicBotId,
  getAllBotIds,
  getSpeechCaptureEnabledStatus,
  getUsersWhoUseVoiceEngine,
  getVoiceListeners,
  getRecordUsers,
  getBotAttentionPhrases,
  getSongCommands,
  getSkipCommands,
  getStopCommands,
  getCancelCommands,
  getAudioPhrases,
  getBannedWords,
  getBannedSongs,
  updateConfig
}