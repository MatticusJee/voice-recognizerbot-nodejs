#!/bin/sh
export $(cat ./data/speech-credentials.env | xargs)
export $(cat ./data/postgres-credentials.env | xargs)
export $(cat ./data/ssl-certificate.env | xargs)

# make sure to run:
# pg_ctl -D postgres_database start
# or
# pg_ctl -D postgres_database -l database_log.txt start
# before this script is executed

if [ -z "$1" ] # or if [ $# -eq 0 ]
  then
    node pm2.js $1
  else
    while test $# -gt 0
    do
        case "$1" in
            --dev)
              node index.js $1 $2 $3
              break
                ;;
            --vosk)
              if [ "$2" = "--dev" ] || [ "$3" = "--dev" ]
              then
                node index.js $1 $2 $3
              else
                node pm2.js $1 $2 $3
              fi
              break
                ;;
              --two_min)
                if [ "$2" = "--dev" ] || [ "$3" = "--dev" ]
                then
                  node index.js $1 $2 $3
                else
                  node pm2.js $1 $2 $3
                fi
                break
                  ;;
            --*) echo "bad argument $1"
                ;;
            *) node pm2.js
                ;;
        esac
        shift
    done
fi

exit 0