const WebSocket = require('ws')
const https = require('https')
const constants = require('constants')
const crypto = require('crypto')
const fs = require('fs')
const path = require('path')
const { AudioSource, getAudioQueue, clearQueue, pauseQueue, resumeQueue, addAudioSourceToQueue } = require('./audio')
const config = require('./config')
const cron = require('./cron')
const db = require('./db')
const Global = require('./Global')
const { printLog } = require('./system')
const Event = require('./Events').eventBus

const processRequest = function (request, response) {
  response.writeHead(200)
  response.end("Request Processed")
}

const server = https.createServer({
  cert: fs.readFileSync(process.env.CERTIFICATE_PATH),
  key: fs.readFileSync(process.env.SSL_PRIVATE_KEY),
  requestCert: true,
  rejectUnauthorized: false,
  enableTrace: false,
  secureOptions: constants.SSL_OP_NO_SSLv3 | constants.SSL_OP_NO_SSLv2,
  secureProtocol: 'SSLv23_method'
}, processRequest).listen(8765)

const wss = new WebSocket.Server({
  server: server,
  host: process.env.HOST,
  path: '/soundboard',
  verifyClient: (info) => {
    if(numOfClients < maxNumOfClients) {
      numOfClients++
      return true
    }
    else {
      return false
    }
  }
})

var numOfClients = 0
const maxNumOfClients = 1
var messageCounter = 0
var websocketClientVoiceConnection = null
var token = ''

// MESSAGE TYPES
const newConnection = 'CONNECTION_ESTABLISHED'
const tokenRefresh = 'TOKEN_REFRESH'
const localPlay = 'LOCAL_PLAY'
const youtubePlay = 'YOUTUBE_PLAY'
const musicLinkPlay = 'MUSIC_FILE_LINK_PLAY'
const musicBotQueue = 'MUSIC_BOT_QUEUE'
const search = 'SEARCH'
const category = 'CATEGORY'
const refreshDatabase = 'DB_REFRESH'
const newSoundbite = 'DB_NEW_SOUNDBITE'
const toggleBotDeafen = 'DISCORD_BOT_DEAFEN'
const toggleUserMute = 'DISCORD_MUTE_USER'
const moveUser = 'DISCORD_MOVE_USER'
const disconnectUser = 'DISCORD_DISCONNECT_USER'
const getUserInfo = 'DISCORD_GET_USER_INFO'
const sendMessageToAllServers = 'DISCORD_BROADCAST_MESSAGE'
const toggleRootSpeechListener = 'CONFIG_TOGGLE_SPEECH_LISTENER_FOR_EVERYONE'
const seeAudioQueue = 'SELF_VIEW_AUDIO_QUEUE'
const clearAudioQueue = 'SELF_CLEAR_AUDIO_QUEUE'
const stopAudio = 'SELF_STOP_AUDIO'
const resumeAudio = 'SELF_RESUME_AUDIO'
const pingUpdate = 'SELF_PING_UPDATE'

wss.on('connection', (ws) => {
  ws.on('message', async (data) => {
    websocketClientVoiceConnection = getVoiceConnectionOfBotOwner()
    const jsonObj = JSON.parse(data)
    if(!jsonObj.hasOwnProperty('message_type')) {
      invalidMessage(null, 'No message_type key found in request')
    }
    else if(!jsonObj.hasOwnProperty('message_id')) {
      invalidMessage(null, 'No message_id key found in request')
    }
    else if(!jsonObj.hasOwnProperty('token')) {
      invalidMessage(jsonObj.message_id, 'Missing token')
    }
    else if(jsonObj.token !== token) {
      invalidMessage(jsonObj.message_id, 'Invalid token')
    }
    else if(jsonObj.message_id <= messageCounter) {
      invalidMessage(jsonObj.message_id, `message_id needs to be > ${messageCounter}`)
    }
    else {
      messageCounter = jsonObj.message_id
      await runRequestType(jsonObj)
    }
  })

  ws.on('ping', (data) => {
    ws.pong()
  })

  ws.on('pong', (data) => {

  })

  ws.on('error', (error) => {
    printLog(`WebSocket error: ${error}`)
  })

  ws.on('close', (data) => {
    console.log('Connection closed')
    numOfClients--
    websocketClientVoiceConnection = null
    messageCounter = 0
    cron.stopRefreshTokenTask()
  })
  cron.startRefreshTokenTask()
  acknowledgeNewConnection()
})

async function runRequestType(jsonObj) {
  const requestType = jsonObj.message_type
  switch(requestType) {
    case localPlay:
      audioLocalPlay(jsonObj)
      break
    case youtubePlay:
      audioYoutubePlay(jsonObj)
      break
    case musicLinkPlay:
      audioMusicLink(jsonObj)
      break
    case musicBotQueue:
      await discordMusicBotQueue(jsonObj)
      break
    case search:
      await dbSearch(jsonObj)
      break
    case category:
      await dbCategory(jsonObj)
      break
    case refreshDatabase:
      await dbRefresh(jsonObj)
      break
    case toggleBotDeafen:
      await discordBotDeafen(jsonObj)
      break
    case toggleUserMute:
      await discordUserMute(jsonObj)
      break
    case moveUser:
      await discordMoveUser(jsonObj)
      break
    case disconnectUser:
      await discordDisconnectUser(jsonObj)
      break
    case getUserInfo:
      await discordGetUserInfo(jsonObj)
      break
    case sendMessageToAllServers:
      discordBroadcastMessage(jsonObj)
      break
    case toggleRootSpeechListener:
      configToggleRootSpeechListener(jsonObj)
      break
    case seeAudioQueue:
      selfViewAudioQueue(jsonObj)
      break
    case clearAudioQueue:
      selfClearAudioQueue(jsonObj)
      break
    case stopAudio:
      selfStopQueue(jsonObj)
      break
    case resumeAudio:
      selfResumeQueue(jsonObj)
      break
    case pingUpdate:
      selfPingUpdate(jsonObj)
      break
    default:
      invalidMessage(jsonObj.message_id,'No matching Request types')
  }
}

function audioLocalPlay(jsonObj) {
  if(!jsonObj.hasOwnProperty('file_path')) {
    invalidMessage(jsonObj.message_id,'No file_path key was found in request')
  }
  else if(jsonObj.file_path === path.basename(jsonObj.file_path)) {
    invalidMessage(jsonObj.message_id,'File path value not valid file path')
  }
  else {
    const filePath = jsonObj.file_path
    if(websocketClientVoiceConnection !== null) {
      addAudioSourceToQueue(websocketClientVoiceConnection, new AudioSource('extension', filePath))
      successfulMessage(jsonObj.message_id, 'Playing Local Audio File')
    }
    else {
      invalidMessage(jsonObj.message_id, 'Either bot or bot owner is not connected to a channel')
    }
  }
}

function audioYoutubePlay(jsonObj) {
  if(!jsonObj.hasOwnProperty('link')) {
    invalidMessage(jsonObj.message_id,'No link key was found in request')
  }
  else if(!isValidURL(jsonObj.link)) {
    invalidMessage(jsonObj.message_id,'This is not a valid link')
  }
  else {
    const youtubeLink = jsonObj.link
    if(websocketClientVoiceConnection !== null) {
      addAudioSourceToQueue(websocketClientVoiceConnection, new AudioSource('youtube', youtubeLink))
      successfulMessage(jsonObj.message_id, 'Playing Youtube Link')
    }
    else {
      invalidMessage(jsonObj.message_id, 'Either bot or bot owner is not connected to a channel')
    }
  }
}

function audioMusicLink(jsonObj) {
  if(!jsonObj.hasOwnProperty('link')) {
    invalidMessage(jsonObj.message_id,'No link key was found in request')
    return
  }
  const musicLink = jsonObj.link
  const fileExtensionIndex = musicLink.lastIndexOf('.')
  const fileExtension = musicLink.substr(fileExtensionIndex, musicLink.length)
  if(!isValidURL(jsonObj.link) && fileExtension !== '.mp3' && fileExtension !== '.wav') {
    invalidMessage(jsonObj.message_id,'This is not a valid link')
  }
  else {
    if(websocketClientVoiceConnection !== null) {
      addAudioSourceToQueue(websocketClientVoiceConnection, new AudioSource('extension', musicLink))
      successfulMessage(jsonObj.message_id, 'Playing Music Link')
    }
    else {
      invalidMessage(jsonObj.message_id, 'Either bot or bot owner is not connected to a channel')
    }
  }
}

async function discordMusicBotQueue(jsonObj) {
  if(!jsonObj.hasOwnProperty('queue')) {
    invalidMessage(jsonObj.message_id,'No queue key was found in request')
    return
  }
  const queue = jsonObj.queue
  if(queue === '') {
    invalidMessage(jsonObj.message_id,'This is not a valid queue request')
  }
  else {
    if(websocketClientVoiceConnection !== null) {
      const server = config.getServer(websocketClientVoiceConnection.channel.guild.id)
      const musicBotTextChannelId = config.getTextChannel(websocketClientVoiceConnection.channel.guild.id)
      const textChannel = await Global.fetchChannel(musicBotTextChannelId)
      textChannel.send(`${server.music_bot_prefix}${server.queue_command} ${queue}`)
      successfulMessage(jsonObj.message_id, 'Song Queued')
    }
    else {
      invalidMessage(jsonObj.message_id, 'Either bot or bot owner is not connected to a channel')
    }
  }
}

async function dbSearch(jsonObj) {
  if(!jsonObj.hasOwnProperty('text')) {
    invalidMessage(jsonObj.message_id,'No text key was found in request')
    return
  }
  const searchString = (jsonObj.text !== '') ? jsonObj.text : "%"
  const keyId = jsonObj.key_id
  const response = await db.search(searchString, keyId)
  if(typeof response === 'undefined' || response === null) {
    invalidMessage(jsonObj.message_id, 'No valid response')
  }
  else {
    successfulDbSearchMessage(jsonObj.message_id, search, searchString, response)
  }
}

async function dbCategory(jsonObj) {
  if(!jsonObj.hasOwnProperty('category')) {
    invalidMessage(jsonObj.message_id,'No category key was found in request')
    return
  }
  const jsonCategoryName = (jsonObj.category !== '') ? jsonObj.category : "%"
  const keyId = jsonObj.key_id
  const response = await db.getUsernameCategory(jsonCategoryName, keyId)
  if(typeof response === 'undefined' || response === null) {
    invalidMessage(jsonObj.message_id, 'No valid response')
  }
  else {
    successfulDbSearchMessage(jsonObj.message_id, category, jsonCategoryName, response)
  }
}

async function dbRefresh(jsonObj) {
  const response = await db.refresh()
  successfulMessage(jsonObj.message_id, response)
}

async function discordBotDeafen(jsonObj) {
  if(!jsonObj.hasOwnProperty('bot_deafen_enabled')) {
    invalidMessage(jsonObj.message_id, 'No set_mute key was found in request')
  }
  else {
    if(websocketClientVoiceConnection !== null) {
      const deafValue = jsonObj.bot_deafen_enabled
      const bot = websocketClientVoiceConnection.channel.members.filter((member) => { return member.user.bot === true && member.user.id === Global.discordClient.user.id }).map((member => member))[0]
      if(typeof bot !== 'undefined') {
        await bot.voice.setDeaf(deafValue)
        const response = (deafValue === true) ? 'Bot deafened' : 'Bot can hear again'
        successfulMessage(jsonObj.message_id, response)
      }
      else {
        invalidMessage(jsonObj.message_id, 'Bot was not found')
      }
    }
  }
}

async function discordUserMute(jsonObj) {
  if(!jsonObj.hasOwnProperty('user_id')) {
    invalidMessage(jsonObj.message_id, 'No user_id key was found in request')
  }
  else if(!jsonObj.hasOwnProperty('user_mute_enabled')) {
    invalidMessage(jsonObj.message_id, 'No set_mute key was found in request')
  }
  else {
    if(websocketClientVoiceConnection !== null) {
      const muteValue = jsonObj.user_mute_enabled
      const userId = jsonObj.user_id
      const user = websocketClientVoiceConnection.channel.members.filter((member) => { return member.user.id === userId }).map((member => member))[0]
      if(typeof user !== 'undefined') {
        await user.voice.setMute(muteValue)
        const response = (muteValue === true) ? 'User muted' : 'User unmuted'
        successfulMessage(jsonObj.message_id, response)
      }
      else {
        invalidMessage(jsonObj.message_id, 'User was not found')
      }
    }
  }
}

async function discordMoveUser(jsonObj) {
  if(!jsonObj.hasOwnProperty('user_id')) {
    invalidMessage(jsonObj.message_id, 'No user_id key was found in request')
  }
  else if(!jsonObj.hasOwnProperty('channel_id')) {
    invalidMessage(jsonObj.message_id, 'No channel_id key was found in request')
  }
  else {
    if(websocketClientVoiceConnection !== null) {
      const userId = jsonObj.user_id
      const channelId = jsonObj.channel_id
      const user = websocketClientVoiceConnection.channel.members.filter((member) => { return member.user.id === userId }).map((member => member))[0]
      if(typeof user !== 'undefined') {
        await user.voice.setChannel(channelId)
        const response = `User successfully moved to channel_id: ${channelId}`
        successfulMessage(jsonObj.message_id, response)
      }
      else {
        invalidMessage(jsonObj.message_id, 'User was not found')
      }
    }
  }
}

async function discordDisconnectUser(jsonObj) {
  if(!jsonObj.hasOwnProperty('user_id')) {
    invalidMessage(jsonObj.message_id, 'No user_id key was found in request')
  }
  else {
    if(websocketClientVoiceConnection !== null) {
      const userId = jsonObj.user_id
      const user = websocketClientVoiceConnection.channel.members.filter((member) => { return member.user.id === userId }).map((member => member))[0]
      if(typeof user !== 'undefined') {
        await user.voice.setChannel(null)
        const response = `User successfully kicked out of the channel`
        successfulMessage(jsonObj.message_id, response)
      }
      else {
        invalidMessage(jsonObj.message_id, 'User was not found')
      }
    }
  }
}

async function discordGetUserInfo(jsonObj) {
  if(!jsonObj.hasOwnProperty('user_id')) {
    invalidMessage(jsonObj.message_id, 'No user_id key was found in request')
  }
  else {
    if(websocketClientVoiceConnection !== null) {
      const userId = jsonObj.user_id
      try {
        const user = await Global.discordClient.users.fetch(userId, false)
        successfulMessageWithData(jsonObj.message_id, getUserInfo, user)
      } catch (error) {
        invalidMessage(jsonObj.message_id, error)
      }
    }
  }
}

function discordBroadcastMessage(jsonObj) {
  if(!jsonObj.hasOwnProperty('message')) {
    invalidMessage(jsonObj.message_id, 'No message key was found in request')
  }
  else {
    if(websocketClientVoiceConnection !== null) {
      const message = jsonObj.message
      Event.emit('Discord', ['sendMessageToServers', `**A message from the great <@${config.getBotOwner()}>:**\n${message}`])
      successfulMessage(jsonObj.message_id, 'Message successfully sent')
    }
  }
}

function configToggleRootSpeechListener(jsonObj) {
  if(!jsonObj.hasOwnProperty('root_speech_listener_enabled')) {
    invalidMessage(jsonObj.message_id, 'No root_speech_listener_enabled key was found in request')
  }
  else {
    const rootSpeechListenerEnabled = jsonObj.root_speech_listener_enabled
    if(websocketClientVoiceConnection !== null && typeof rootSpeechListenerEnabled === 'boolean') {
      Event.emit('Discord', ['setRootSpeechListener', rootSpeechListenerEnabled])
      successfulMessage(jsonObj.message_id, `Speech Listener set to ${(rootSpeechListenerEnabled === true) ? 'ON' : 'OFF'} for everyone`)
    }
  }
}

function selfViewAudioQueue(jsonObj) {
  if(websocketClientVoiceConnection !== null) {
    const queue = getAudioQueue()
    successfulMessageWithData(jsonObj.message_id, seeAudioQueue, queue)
  }
}

function selfClearAudioQueue(jsonObj) {
  if(websocketClientVoiceConnection !== null) {
    clearQueue()
    successfulMessage(jsonObj.message_id, 'Successfully cleared queue')
  }
}

function selfStopQueue(jsonObj) {
  if(websocketClientVoiceConnection !== null) {
    pauseQueue()
    successfulMessage(jsonObj.message_id, 'Successfully paused queue')
  }
}

function selfResumeQueue(jsonObj) {
  if(websocketClientVoiceConnection !== null) {
    resumeQueue()
    successfulMessage(jsonObj.message_id, 'Successfully resumed queue')
  }
}

function selfPingUpdate(jsonObj) {
  if(websocketClientVoiceConnection !== null) {
    let response = {
      channel: websocketClientVoiceConnection.channel,
      server: websocketClientVoiceConnection.channel.guild,
      members: websocketClientVoiceConnection.channel.guild.members.cache,
      voice_channels: websocketClientVoiceConnection.channel.guild.channels.cache.filter(channel => channel.type === 'voice'),
      text_channels: websocketClientVoiceConnection.channel.guild.channels.cache.filter(channel => channel.type === 'text'),
      bot_configuration: config.getConfig()
    }
    successfulMessageWithData(jsonObj.message_id, pingUpdate, response)
  }
}

function acknowledgeNewConnection() {
  token = generateToken()
  messageCounter = messageCounter += 1
  let jsonMessage = {
    message_id: messageCounter,
    code: 200,
    message_type: newConnection,
    data: {
      refresh_token: token
    }
  }
  sendMessage(JSON.stringify(jsonMessage))
}

async function dbNewData(data) {
  if(wss.clients.size <= 0) return
  messageCounter = messageCounter += 1
  let jsonMessage = {
    message_id: messageCounter,
    code: 200,
    message_type: newSoundbite,
    data: data
  }
  sendMessage(JSON.stringify(jsonMessage))
}

function successfulMessage(messageId, message) {
  let jsonResponse = {
    message_id: messageId,
    code: 200,
    message: message
  }
  sendMessage(JSON.stringify(jsonResponse))
}

function successfulMessageWithData(messageId, messageType, response) {
  let jsonMessage = {
    message_id: messageId,
    code: 200,
    message_type: messageType,
    data: response,
  }
  sendMessage(JSON.stringify(jsonMessage))
}

function successfulDbSearchMessage(messageId, messageType, text, response) {
  let jsonMessage = {
    message_id: messageId,
    code: 200,
    message_type: messageType,
    text: text,
    data: response,
  }
  sendMessage(JSON.stringify(jsonMessage))
}

function invalidMessage(messageId, error) {
  let jsonResponse
  if(typeof messageId === 'undefined' || messageId === null) {
    jsonResponse = {
      code: 400,
      error: error
    }
  }
  else {
    jsonResponse = {
      message_id: messageId,
      code: 400,
      error: error
    }
  }
  sendMessage(JSON.stringify(jsonResponse))
}

function sendMessage(response) {
  for(client of wss.clients) {
    if (client !== wss && client.readyState === WebSocket.OPEN) {
      client.send(response)
    }
  }
}

function isValidURL(str) {
  const pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  return !!pattern.test(str);
}

function getVoiceConnectionOfBotOwner() {
  for(voiceConnection of Global.listeningVoiceConnections) {
    const voiceChannels = Global.getListOfAvailableVoiceChannelsInServer(voiceConnection.channel.guild)
    for(voiceChannel of voiceChannels) {
      const membersInChannel = voiceChannel.members.filter((member) => { return member.user })
      for(const [memberId, guildMember] of membersInChannel) {
        if(memberId === config.getBotOwner()) {
          return voiceConnection
        }
      }
    }
  }
  return null
}

Event.on('WebSocket', async (args) => {
  if(args.length <= 0) { return }
  const functionName = args[0]
  switch(functionName) {
    case 'refreshToken':
      refreshToken()
      break
    case 'newSoundbite':
      const jsonObj = args[1]
      await dbNewData(jsonObj)
      break
  }
})

function refreshToken() {
  token = generateToken()
  messageCounter = messageCounter += 1
  successfulMessageWithData(messageCounter, tokenRefresh, { refresh_token: token })
  printLog('TOKEN REFRESHED')
}

function generateToken() {
  return crypto.randomBytes(64).toString('hex')
}
