const googleCloudSpeech = require('@google-cloud/speech')
const microsoftCognitiveSpeech = require("microsoft-cognitiveservices-speech-sdk")
const SpeechToTextV1 = require('ibm-watson/speech-to-text/v1')
const { IamAuthenticator } = require('ibm-watson/auth')
const stream = require('stream').Transform
const os = require('os')
const config = require('./config')
const dateFormat = require('dateformat')
const { sampleRate, bitDepth, ConvertTo1ChannelStream, generateRandomAudioFile, playAudioFileExtension, bufferShouldBeEncoded, encodeAudioData } = require('./audio')
const db = require('./db')
const Global = require('./Global')
const speechTracker = require('./speechTracker')
const { printLog, printError, sleep } = require('./system')
const Event = require('./Events').eventBus

// Google Speech Recognition
const googleSpeechClient = new googleCloudSpeech.SpeechClient()

// Microsoft Speech Recognition
const speechConfig = microsoftCognitiveSpeech.SpeechConfig.fromSubscription(process.env.MICROSOFT_API_KEY, 'eastus')
speechConfig.setProfanity(microsoftCognitiveSpeech.ProfanityOption.Raw)

// Watson Speech Recognition
const watsonSpeechClient = new SpeechToTextV1({
  authenticator: new IamAuthenticator({ apikey: process.env.IBM_SPEECH_TO_TEXT_APIKEY }),
  serviceUrl: process.env.IBM_SPEECH_TO_TEXT_URL
})

// Vosk Speech Recognition
var vosk = null
var voskRecognizer = null
if(os.platform() === 'linux' && process.argv.indexOf('--vosk') > -1) {
  vosk = require('vosk')
  vosk.setLogLevel(0)
  const model = new vosk.Model("model")
  voskRecognizer = new vosk.Recognizer(model, sampleRate)
}

const songCommands = config.getSongCommands()
const skipCommands = config.getSkipCommands()
const stopCommands = config.getStopCommands()
const cancelCommands = config.getCancelCommands()
const audioPhrases = config.getAudioPhrases()

var usersRequestingBot = {} // key == userId, value == boolean
var speechDisabled = false

const monthYearFormat = 'mm_yyyy'

// ===================PUBLIC=================== //
function voiceRecognitionStream(audioStream, connection, user) {
  var audioData = []
  const monthYear = dateFormat(Date(), monthYearFormat)
  const startTimeInMilliseconds = new Date().getTime()
  const convertTo1ChannelStream = new ConvertTo1ChannelStream()
  const request = {
    config: {
      encoding: 'LINEAR16',
      sampleRateHertz: sampleRate,
      languageCode: 'en-US'
    }
  }
  const googleFreeTime = parseInt(speechTracker.getGoogleFreeTime())
  const microsoftFreeTime = parseInt(speechTracker.getMicrosoftFreeTime())
  const watsonFreeTime = parseInt(speechTracker.getWatsonFreeTime())
  const totalTalkingTime = (typeof speechTracker.getTotalSpeechTimeOfMonth(monthYear) !== 'undefined') ? parseInt(speechTracker.getTotalSpeechTimeOfMonth(monthYear)) : 0
  var transcription = 'undefined'
  if(totalTalkingTime <= microsoftFreeTime - 4000) {
    Global.speechEngine = 'Microsoft'
    speechDisabled = false
    const format = microsoftCognitiveSpeech.AudioStreamFormat.getWaveFormatPCM(sampleRate, bitDepth, 1)
    const microsoftStream = microsoftCognitiveSpeech.AudioInputStream.createPushStream(format)
    let audioConfig = microsoftCognitiveSpeech.AudioConfig.fromStreamInput(microsoftStream)
    let recognizer = new microsoftCognitiveSpeech.SpeechRecognizer(speechConfig, audioConfig)
    recognizer.recognizeOnceAsync(async (result) => {
      transcription = (typeof result !== 'undefined' && typeof result.text !== 'undefined') ? result.text.toLowerCase() : 'undefined'
      printLog(`Microsoft Transcription from ${user.username}: ${transcription}`)
    })
    audioStream.pipe(convertTo1ChannelStream).on('data', (arrayBuffer) => {
      microsoftStream.write(arrayBuffer)
    }).on('error', (error) => {
      printError(`MICROSOFT_CLOUD_ERROR: ${error}`)
      Global.speechEngine = 'IBM Watson'
    }).on('end', () => {
      microsoftStream.close()
    })
  }
  else if(totalTalkingTime <= microsoftFreeTime + watsonFreeTime - 8000) {
    Global.speechEngine = 'IBM Watson'
    speechDisabled = false
    const watsonRecognizeStream = watsonSpeechClient.recognizeUsingWebSocket( { contentType: 'audio/l16; rate=48000', profanityFilter: false }).setEncoding('utf8').on('data', async (transcript) => {
      transcription = transcript.toLowerCase()
      printLog(`IBM_Watson Transcription from ${user.username}: ${transcription}`)
    }).on('error', (error) => {
      printError(`IBM_WATSON_CLOUD_ERROR: ${error}`)
      Global.speechEngine = 'Google'
    })
    audioStream.pipe(convertTo1ChannelStream).pipe(watsonRecognizeStream)
  }
  else if(totalTalkingTime <= microsoftFreeTime + watsonFreeTime + googleFreeTime - 12000) {
    Global.speechEngine = 'Google'
    speechDisabled = false
    const googleRecognizeStream = googleSpeechClient.streamingRecognize(request).on('error', (error) => {
      printError(`GOOGLE_CLOUD_ERROR: ${error}`)
      if(`${error}`.indexOf('PERMISSION_DENIED') > -1) {
        Global.speechEngine = 'Vosk'
      }
    }).on('data', async (response) => {
      transcription = response.results.map(result => result.alternatives[0].transcript).join('\n').toLowerCase()
      printLog(`Google Transcription from ${user.username}: ${transcription}`)
    })
    audioStream.pipe(convertTo1ChannelStream).pipe(googleRecognizeStream)
  }
  else if(vosk !== null) {
    Global.speechEngine = 'Vosk'
    vosk.setLogLevel(0)
    const writableStream = new stream.Writable({
      write(chunk, encoding, callback) {
        voskRecognizer.acceptWaveform(chunk)
        callback(null)
      }
    })
    writableStream.on('finish', async () => {
      const result = JSON.parse(voskRecognizer.result())
      transcription = (result.text !== '') ? result.text : 'undefined'
      printLog(`Vosk Transcription from: ${transcription}`)
    })
    audioStream.pipe(convertTo1ChannelStream).pipe(writableStream)
  }
  else if(speechDisabled === false) {
    Global.speechEngine = 'N/A'
    speechDisabled = true
    Event.emit('Discord', ['sendMessageToServers', 'Voice commands are **DISABLED**! Everyone will have to wait til next month to use me for voice commands'])
  }
  audioStream.on('error', (error) => {
    printError(`AUDIOSTREAM_ERROR -1: ${error}`)
  })
  audioStream.on('data', (buffer) => {
    audioData.push(buffer)
  })
  audioStream.on('end', async () => {
    printLog(`Stopped listening to ${user.username}`)
    const endTimeInMilliseconds = new Date().getTime()
    const totalSpeechTimeInMilliseconds = endTimeInMilliseconds - startTimeInMilliseconds
    speechTracker.updateSpeechTime(monthYear, user.id, totalSpeechTimeInMilliseconds, Global.speechEngine)
    // IMPORTANT: handleTranscription is needed here instead of where 'transcription' is set because sometimes there is no callback for audio that is 'undefined'
    sleep(500).then(async () => {
      await handleTranscription(transcription, audioData, user, connection)
      console.log()
    })
  })
}

async function handleTranscription(transcription, audioData, user, connection) {
  var isCmd = false
  transcription = transcription.replace('/', '-').replace(':', '_').replace('"', '').replace('?', '.').replace('%', '').replace('.', '')
  const serverId = connection.channel.guild.id
  const { isKeyPhrase, file } = keyPhraseWasSaid(transcription)
  const { saidBadWord, highlightedString } = badWordWasSaid(transcription)
  if(botIsRequested(transcription)) {
    printLog("Bot Requested")
    generateRandomAudioFile(connection, "greetings")
    usersRequestingBot[user.id] = true
    isCmd = true
  }
  else if(usersRequestingBot[user.id] === true) {
    const server = config.getServer(serverId)
    try {
      const channel =  await Global.fetchChannel(server.channel_id)
      const { isSongCmd, cmd, index } = phraseIsSongCommand(transcription)
      const isSkipCmd = phraseIsSkipCommand(transcription)
      const isStopCmd = phraseIsStopCommand(transcription)
      const isCancelCmd = phraseIsCancelCommand(transcription)
      if(isSongCmd) {
        if(server.queue_command !== null && server.music_bot_prefix !== null) {
          const formattedString = transcription.substr(index).replace(cmd,'')
          channel.send(`${server.music_bot_prefix}${server.queue_command}${formattedString}`)
          usersRequestingBot[user.id] = false
          isCmd = true
        }
        else {
          channel.send('Queue command/Prefix is not set!')
        }
      }
      else if(isSkipCmd) {
        if(server.skip_command !== null) {
          channel.send(`${server.music_bot_prefix}${server.skip_command}`)
          usersRequestingBot[user.id] = false
          isCmd = true
        }
        else {
          channel.send('Skip command is not set!')
        }
      }
      else if(isStopCmd) {
        if(server.stop_command !== null) {
          channel.send(`${server.music_bot_prefix}${server.stop_command}`)
          usersRequestingBot[user.id] = false
          isCmd = true
        }
        else {
          channel.send('Stop command is not set!')
        }
      }
      else if(isCancelCmd) {
        usersRequestingBot[user.id] = false
        isCmd = true
      }
    } catch (error) {
      printLog(error)
      Event.emit('Command', ['restartCommand'])
    }
  }
  else if(isKeyPhrase && !Global.musicBotIsSpeaking) {
    playAudioFileExtension(connection, `./data/${file}`)
  }
  else if(saidBadWord) {
    printLog("Punishment must fit the crime")
    for(server of Global.guildMembers) {
      for(const [id, member] of server) {
        if(id === user.id) {
          Event.emit('Discord', ['punishUser', member])
          Event.emit('Discord', ['sendMessageToUser', id, `You're punished for saying: '${highlightedString}'`])
        }
      }
    }
  }
  try {
    if(bufferShouldBeEncoded(audioData, user.id, transcription, isCmd)) {
      const {userId, username, fileName, filePath, createdDate} = await encodeAudioData(audioData, user, transcription)
      console.log(`User Id: ${userId}`)
      console.log(`Username Id: ${username}`)
      console.log(`Filename: ${fileName}`)
      console.log(`Filepath: ${filePath}`)
      console.log(`Created Date: ${createdDate}`)
      await db.addFile(userId, username, fileName, filePath, createdDate)
      Event.emit('System', ['getDiskInfo'])
    }
  } catch (error) {
    printLog(error)
    throw error
  }
}

function botIsRequested(phrase) {
  const requestBotPhrases = config.getBotAttentionPhrases()
  return new RegExp(requestBotPhrases.join("|")).test(phrase)
}

function keyPhraseWasSaid(phrase) {
  const phrases = Object.keys(audioPhrases)
  for(keyPhrase of phrases) {
    if(phrase.indexOf(keyPhrase) > -1) {
      const audioFiles = Object.values(audioPhrases[keyPhrase])
      const audioFile = audioFiles[Math.floor(Math.random() * audioFiles.length)]
      return { isKeyPhrase: true, file: audioFile }
    }
  }
  return { isKeyPhrase: false, file: null }
}

function badWordWasSaid(phrase) {
  const bannedWords = config.getBannedWords()
  for(bw of bannedWords) {
    if(phrase.indexOf(bw) > -1) {
      const regex = new RegExp(`(\\s${bw})+`, 'g')
      const regex2 = new RegExp(`${bw}`, 'g')
      const modifiedString0 = phrase.replace(regex, ` **${bw}**`)
      const modifiedString1 = phrase.replace(regex2, `**${bw}**`)
      const lastIndexOfBoldChar = modifiedString1.lastIndexOf('**')
      const charAfterBold = modifiedString1.charAt(lastIndexOfBoldChar + 2)
      if(typeof regex !== 'undefined' && (charAfterBold === '' || charAfterBold === ' ')) {
        if(modifiedString0.indexOf('**') > -1) {
          return { saidBadWord: true, highlightedString: modifiedString0 }
        }
        else if(typeof regex2 !== 'undefined' && modifiedString1.indexOf('**') > -1) {
          return { saidBadWord: true, highlightedString: modifiedString1 }
        }
      }
    }
  }
  return { saidBadWord: false, highlightedString: null }
}

function phraseIsSongCommand(phrase) {
  for(cmd of songCommands) {
    const cmdIndex = phrase.indexOf(cmd)
    if(cmdIndex > -1) {
      return { isSongCmd: true, cmd: cmd, index: cmdIndex }
    }
  }
  return { isSongCmd: false, cmd: null, index: null }
}

function phraseIsSkipCommand(phrase) {
  for(cmd of skipCommands) {
    if(phrase.indexOf(cmd) > -1) {
      return true
    }
  }
  return false
}

function phraseIsStopCommand(phrase) {
  return stopCommands.indexOf(phrase) > -1
}

function phraseIsCancelCommand(phrase) {
  return cancelCommands.indexOf(phrase) > -1
}

module.exports = {
  voiceRecognitionStream
}