const cron = require('node-cron')
const { broadcastAudioFileExtension } = require('./audio')
const { backupDataFolder, printLog } = require('./system')
const Event = require('./Events').eventBus

// Update Longest Uptime streak
// cron.schedule('1 * * * *', () => {
//   console.log("Update Uptime")
// })

// Backup data folder once a month
cron.schedule('*/5 * * * *', () => {
  backupDataFolder()
})

const refreshTokenTask = cron.schedule('*/30 * * * *', () => { // Refresh every 30 minutes when websocket client is connected
  Event.emit('WebSocket', ['refreshToken'])
})
refreshTokenTask.stop()

// # ┌────────────── second (optional)
// # │ ┌──────────── minute
// # │ │ ┌────────── hour
// # │ │ │ ┌──────── day of month
// # │ │ │ │ ┌────── month
// # │ │ │ │ │ ┌──── day of week
// # │ │ │ │ │ │
// # │ │ │ │ │ │
// # * * * * * *
// Piano Man by Billy Joel
function pianoManListener() {
  cron.schedule('29 59 20 * * 6', () => { // Saturday, 8:59:29 pm
    printLog('Playing Piano Man by Billy Joel')
    broadcastAudioFileExtension('./data/Billy Joel - Piano Man.mp3')
  })
}

function oneMoreSaturdayNightListener() {  // :) mikemacs first code
  cron.schedule('38 55 23 * * 6', () => { // Saturday, 11:55:38 pm   
    printLog('Playing One More Saturday Night by Grateful Dead')
    broadcastAudioFileExtension('./data/One More Saturday Night - Grateful Dead.mp3')
  })
}

function startRefreshTokenTask() {
  refreshTokenTask.start()
}

function stopRefreshTokenTask() {
  refreshTokenTask.stop()
}

module.exports = {
  startRefreshTokenTask,
  stopRefreshTokenTask,
  pianoManListener,
  oneMoreSaturdayNightListener
}