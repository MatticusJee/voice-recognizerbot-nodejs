const fs = require('fs')
const { printLog, printError } = require('./system')
const speechPath = './data/speech_tracker.json'

var Speech = getSpeechInfo()

function getSpeechInfo() {
  return JSON.parse(fs.readFileSync(speechPath, "utf-8"))
}

function getGoogleFreeTime() {
  return Speech.google_free_time
}

function getMicrosoftFreeTime() {
  return Speech.microsoft_free_time
}

function getWatsonFreeTime() {
  return Speech.ibm_watson_free_time
}

function getTotalFreeTime() {
  return Speech.total_free_time
}

function getUserSpeechTimeOfMonth(month, userId) {
  if(typeof Speech[month] === 'undefined') {
    Speech[month] = { total_talking_time: '0' }
  }
  return Speech[month][userId]
}

function getTotalSpeechTimeOfMonth(month) {
  if(typeof Speech[month] === 'undefined') {
    Speech[month] = { total_talking_time: '0' }
  }
  return Speech[month].total_talking_time
}

function updateSpeechTime(month, userId, addedSpeechTime, currentSpeechEngine) {
  const userSpeechTimeBefore = getUserSpeechTimeOfMonth(month, userId)
  const beforeAsNumber = (typeof userSpeechTimeBefore !== 'undefined') ? parseInt(userSpeechTimeBefore) : 0
  const userSpeechTimeAfter = beforeAsNumber + addedSpeechTime
  Speech[month][userId] = userSpeechTimeAfter.toString()
  const totalMonthTimeBefore = parseInt(Speech[month].total_talking_time)
  Speech[month].total_talking_time = (totalMonthTimeBefore + addedSpeechTime).toString()
  Speech.current_voice_engine = currentSpeechEngine
  updateSpeechInfoFile()
}

function updateSpeechInfoFile() {
  fs.writeFile(speechPath, JSON.stringify(Speech, null, 2), (error) => {
    if(error) {
      printLog(`UPDATE_SPEECH_FILE_ERROR: ${error}`)
    }
    else {
      printLog(`SUCCESS!`)
      // console.log(Speech)
    }
  })
}

module.exports = {
  Speech,
  getGoogleFreeTime,
  getMicrosoftFreeTime,
  getWatsonFreeTime,
  getTotalFreeTime,
  getUserSpeechTimeOfMonth,
  getTotalSpeechTimeOfMonth,
  updateSpeechTime
}